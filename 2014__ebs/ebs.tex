\documentclass[twocolumn]{article}

\usepackage{amsmath,amssymb}

\DeclareMathOperator*{\argmin}{arg\,min}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\interval}[2]{\left[\,#1\,\middle|\,#2\,\right]}

\newcommand{\refeq}[1]{eq. \ref{eq:#1}}

\begin{document}

\title{Event-based stereo vision}

\maketitle


\section{Abstract}

TODO


\section{Introduction}

\begin{itemize}
\item Event-based vs. frame-based
\item Previous work on event-based vision
\item Importance of stereo vision
\item Short overview over dense stereo vision
\end{itemize}


\section{Event flow}

The event time field represents the tempo-spatial surface of events in image space.
The gradient $\nabla T = (\partial_i T, \partial_j T)$ of the time field represents the change of time values with respect to image coordinates.
At the same time it can be used to locally predict the occurrence of new events and thus represents the flow of event.

The computation of the gradient is not trivial as the time field is not continuous everywhere due to the nature of event generation.
Actually almost every new event is a point of discontinuity.
This can be seen with a simple example: 
Picture a perfectly vertical line moving from left to right in front of the image sensor.
Events are generated exactly on the line and the time field is only continuous to the left where previous events are located.
To the right unrelated time information is stored which can produce arbitrary gradients not related to the current motion. 
This is rather unfavourable as we want to compute the event flow exactly for every new event.

For a given pixel location $(i,j)$ we compute the gradient by a weighted plane fit over a local patch $B_R((i,j),T)$.
The weight associated with each pixel $(k,l) \in B_R((i,j),T)$ is computed as
\begin{equation}
	w_{kl} = \exp\left(-\tfrac{1}{2\sigma^2}\left(t_{kl} + f_{kl}\circ(i-k,j-l) - t_{ij}\right)^2\right)
\end{equation}
Here $f_{kl}$ is the flow previously computed for location $(k,l)$, thus $t_{kl} + f_{kl}\circ(i-k,j-l)$ is the time expected for the current location $(i,j)$.
The weight thus tolerates only a certain deviation between expected and actual time $t_{ij}$ before it diminishes the influence of a pixel location on the gradient computation.


\section{Local event-based disparity}

\newcommand{\sps}{\vec{s}}
\newcommand{\spt}{\vec{t}}
\newcommand{\sptd}[1]{\vec{t}_{[#1]}}
\newcommand{\sdmax}{d_\text{max}}

The main idea of our disparity computation is to use an event time field $T$ for local matching.
The event time field stores for each pixel location the timestamp of the most recent event:
\begin{equation}
	T_{ij}^{(k)} := \max\left\{t\,\middle|\,(i,j,t)\in E^{(k)}\right\}
\end{equation}
The event time field thus represents a projection of the tempo-spatial surface of intensity boundaries as seen form the sensor.
We can not represent the whole surface as new events will override old information.

To compute disparities for a given event at location $(i,j)$, we look at a rectangular patch $B((i,j),T) := (T_{lk})_{i-R \le l \le i+R, j-R \le k \le j+R}$ of the event time field of the left camera and try to find the closest matching patch in the time field of the right camera.
Here $R\in\mathbb{N}$ is the ''radius'' of the patch and thus $2R+1$ its size length.
The disparity is the necessary shift $d$ which aligns the local patch $\sps := B((i,j),T_L)$ in the left event time fields as close as possible to the shifted patch $\sptd{d} := B((i,j+d),T_R)$ of the right time field.
Thus we want to compute
\begin{equation}
	\label{eq:dmin0}
	d^{*} := \argmin_{d\in\{0,\ldots,d_\text{max}\}}{E_0(d)}
\end{equation}
where the error function is
\begin{equation}
	 E_0(d) := \left\|\sps-\sptd{d}\right\|_2, d \in \mathbb{N}
\end{equation}

Here we chose $\|\cdot\|_2$ to be the sum of square difference of time values.
This is a similar technique as used in classic dense stereo vision, where the absolute or squared difference or gradient measures over patches are used to compute disparities.
As events represent changes in the image, our error function corresponds to a gradient based measure in the sense of classic dense stereo computation.
An intensity based measure can not be used as absolute intensity values are not available.

While this approach is simple to implement and already yields quite good results, a difference of a single pixel in disparity can have a large impact on the change in depth.
To get a lower depth error we compute the disparity with subpixel accuracy.
Hence we allow $d \in \interval{0}{\sdmax} \subset \mathbb{R}$ and evaluate $E_0$ for non-integral disparities by using linear interpolation:
\begin{equation}
	E_1(d) := \left\|(1-d_p)\,\sptd{d_i}+d_p\,\sptd{d_i+1}-\sps\right\|_2, \, d \in \mathbb{R}
\end{equation}
$d_i$ and $d_p$ are computed such that $d_i+d_p=d$, $d_i \in \mathbb{N}$ and $0 \le d_p \le 1$.
The minimization problem in \refeq{dmin0} is thus extended to:
\begin{equation}
	d^{*} := \argmin_{d\in \interval{0}{\sdmax}}{E_1(d)}
\end{equation}

For a fixed $d_i$ the optimal $d_p^{*}$ can be computed as
\begin{equation}
	d_p^{*} = \frac{(\sps-\sptd{d_i})(\sptd{d_i+1}-\sptd{d_i})}{(\sptd{d_i+1}-\sptd{d_i})^2}
\end{equation}
% and the corresponding error as
% \begin{equation}
% 	% E_1(\sps,\spt,d_i+d_p^{*}) = (\sps-\sptd{d})^2 - 2\,d_p\,(\sps-\sptd{d})(\sptd{d+1}-\sptd{d}) + p^2\,(\sptd{d+1}-\sptd{d})^2
% 	E_1(\sps,\spt,d_i+d_p^{*}) = \left\|(\sptd{d_i}-\sps) + d_p^{*}\,(\sptd{d_i+1}-\sptd{d_i})\right\|_2
% \end{equation}
In the unlikely case that $\sptd{d} = \sptd{d+1}$, $d_p^{*}$ can be chosen to be $\tfrac{1}{2}$.
In total we perform an iteration over $d_i \in \{0,\ldots,\sdmax\}$ and for each $d_i$ compute the optimal $d_p^{*}$.
From all theses possible minima we only consider those where $0 \le d_p^{*} \le 1$ and find the one with the smallest error $E_1(d_i+d_p^{*})$.


\section{Global event-based disparity}

% \section{Estimation of dense stereo information}

% Stereo information can be completed by finding a minimal surface which satisfies given depth values for the boundary $C$.

% \begin{equation}
% 	\int_{p \in Omega}{\| \nabla \varphi(p) \|^2} \overset{!}{=} \min \text{ and } \varphi(p) = d(p), p \in C \subset \Omega
% \end{equation}

% By discretizing the image domain $\Omega$ to a grid $I := \{1,\ldots,n\}\times\{1,\ldots,m\}$ with values $u_{ij}$, we get
% \begin{equation}
% 	\sum_{i,j}{\left[ (u_{i+1,j}-u_{i,j})^2 + (u_{i,j+1}-u_{i,j})^2\right]} \overset{!}{=} 0 \text{ and } u_p = d_p, p \in C \subset I
% \end{equation}

% This can be solved to get the iterative equations
% \begin{equation}
% 	u_{ij} = \begin{cases}
% 		\tfrac{1}{4}\left(u_{i,j+1} + u_{i,j-1} + u_{i+1,j} + u_{i-1,j}\right) & \text{ if } (i,j) \in I-C \\
% 		d_{ij} & \text{ if } (i,j) \in C
% 	\end{cases}
% \end{equation}


\section{Experimental results}

\begin{itemize}
\item Quantitatively compare to Kinect (??)
\begin{itemize}
\item Use 14 cm base line
\item Calibrate Kinect onto event-based
\item Detect chess board in event-based
\end{itemize}
\item Qualitatively compare to Kinect (?)
\item Compare algorithm parameters
\end{itemize}


\section{Conclusion}

TODO


\end{document}
