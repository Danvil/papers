## Publications

# 2014

[UNDER CONSTRUCTION] ?? Event-based Stereo Matching

[UNDER CONSTRUCTION] ?? Event-based camera calibration and a ground-truth annotated stereo dataset for Dynamic Vision Sensors

[UNDER CONSTRUCTION] FRONTIERS Event-based Computer Vision

[UNDER CONSTRUCTION] **D. Weikerdorfer**, J. Conradt, "Fully integrated, autonomous flight control for micro aerial vehicles using event-based SLAM", submitted to *EURASIP Journal on Image and Video Processing, Special Issue Vision Systems and Algorithms for Micro Aerial Vehicles*, 2014

Daniel Clarke, **D. Weikersdorfer**, Alois Knoll, "Event-Based Low-Latency Computer Vision: An Opportunity for Autonomous Vehicles", submitted to *International IEEE Conference on Intelligent Transportation Systems*, Qingdao, 2014

**D. Weikersdorfer**, D.B. Adrian, D. Cremers and J. Conradt, "Event-based 3D SLAM with a depth-augmented dynamic vision sensor", *International Conference on Robotics and Automation (ICRA)*, Hong-Kong, 2014

# 2013

R. Hoffmann, **D. Weikersdorfer**, and J. Conradt, "Autonomous Indoor Exploration with an Event-Based Visual SLAM System", in *Proc. European Conference on Mobile Robots*, Barcelona, 2013

**D. Weikersdorfer**, R. Hoffmann, and J. Conradt, "Simultaneous Localization and Mapping for event-based Vision Systems", in *Proc. International Conference on Computer Vision Systems*, St. Petersburg, 2013

**D. Weikersdorfer**, A. Schick, and D. Cremers, "Temporal Depth-adaptive Supervoxel for efficient RGB-D video analysis", in *Proc. International Conference on Image Processing*, Melbourne, 2013

# 2012

**D. Weikersdorfer** and J. Conradt, "Event-based particle filtering for robot self-localization", in *Proc. International Conference on Robotics and Biomimetics*, pp. 866 - 870, Guangzhou, 2012

**D. Weikersdorfer**, D. Gossow, and M. Beetz, "Depth-Adaptive Superpixels", in *Proc. International Conference on Pattern Recognition*, pp. 2087 - 2090, Tokyo, 2012

D. Gossow, **D. Weikersdorfer**, and M. Beetz, "Distinctive Texture Features from Perspective-Invariant Keypoints", in *Proc. International Conference on Pattern Recognition*, pp. 2764 - 2767, Tokyo, 2012
