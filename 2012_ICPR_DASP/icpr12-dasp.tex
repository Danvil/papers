%
%  $Description: Author guidelines and sample document in LaTeX 2.09$
%
%  $Author: ienne $
%  $Date: 1995/09/15 15:20:59 $
%  $Revision: 1.4 $
%

\documentclass[10pt,twocolumn]{article}
\usepackage{latex12}
\usepackage{times}
\usepackage{balance}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb} % define this before the line numbering.
\usepackage{subfigure}
\usepackage{graphicx}
\usepackage{multirow}

%\documentstyle[times,art10,twocolumn,latex8]{article}

\newcommand{\depth}{\zeta}
\newcommand{\ppix}{p_j}
\newcommand{\ppiy}{p_i}
\newcommand{\ppnt}{p_v}
\newcommand{\pdth}{p_{\depth}}
\newcommand{\pcol}{p_c}
\newcommand{\pnrm}{p_n}
%\newcommand{\incgfx}[2]{\includegraphics[trim=40px 20px 50px 40px, clip, width=#1]{#2}}
\newcommand{\incgfx}[2]{\includegraphics[trim=20px 10px 25px 20px, clip, width=#1]{#2}}
\newcommand{\incplt}[2]{\includegraphics[width=#1]{#2}}

\title{Depth-Adaptive Superpixels}

\author{David Weikersdorfer, David Gossow, Michael Beetz\\
\emph{Intelligent Autonomous Systems Group, Technische Universität München.}\\
\emph{[weikersd|gossow|beetz]@cs.tum.edu}\\
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
%\and
%Second Author\\
%Institution2\\
%First line of institution2 address\\ Second line of institution2 address\\
%SecondAuthor@institution2.com\\
}

%-------------------------------------------------------------------------
% take the % away on next line to produce the final camera-ready version
\pagestyle{empty}

%-------------------------------------------------------------------------
\begin{document}

\maketitle

\thispagestyle{empty}

%-------------------------------------------------------------------------
\begin{abstract}
We propose a novel oversegmentation technique for RGB-D images.
The visible surface of the 3D geometry is partitioned into uniformly distributed and equally sized planar patches.
This results in a classic oversegmentation of pixels into depth-adaptive superpixels which correctly reflect deformation through perspective projection.
%The algorithm is highly efficient and produces a spatial and temporal stable segmentation which enables it for usage in realtime tracking applications.
The advantages of depth-adaptive superpixels (DASP) are demonstrated by using spectral graph theory to create image segmentations in near realtime.
Our algorithms outperform state-of-the-art oversegmentation and image segmentation algorithms both in quality and runtime.
%We evaluate on a small RGB-D dataset and compared against classical superpixel algorithms.
\end{abstract}


%-------------------------------------------------------------------------
\section{Introduction}

Following \cite{Ren2003}, image segmentation divides an image into regions which fulfill two criteria: intra-region similarity and inter-region dissimilarity. Oversegmentation into superpixels focuses on intra-region similarity, possibly dividing an image into more segments than necessary.
An oversegmentation greatly reduces scene complexity and can be used as the basis of advanced and expensive algorithms.
Recent oversegmentation algorithms like Turbopixels \cite{Levinshtein2009} and SLIC \cite{Achanta2010} additionally have the property that the image is covered uniformly with superpixels of similar size. Turbopixels use a geometric-flow-based algorithm which in addition assures connectivity and compactness. 
%The latter two criterions may by violated locally in SLIC.

Since the advent of the Microsoft Kinect device \cite{shotton2011bodypartrecognition}, RGB-D sensors have become available for a wide range of applications.
In this paper we contribute two algorithms, oversegmentation (DASP) and segmentation (sDASP), that make use of the additional depth information in order to simplify the segmentation task (see figure \ref{fig:teaser}).
In section \ref{ref:dasp1} and \ref{sec:dasp}, we describe an oversegmentation algorithm, which partitions the visible scene geometry into uniformly distributed near-planar surface patches.
Due to its efficiency, we keep the notion of a two-dimensional image and instead deform the shape of local neighborhoods in image space according to the perspective distortion and scaling.
%Resulting superpixels in the image plane are depth-adaptive as their size and shape depend on the position and orientation of the corresponding points in space.
\begin{figure}
	\label{fig:teaser}
	\subfigure[Color input image]{\incgfx{0.48\columnwidth}{images/teaser_color_small.jpg}}
	\subfigure[Depth input values]{\incgfx{0.48\columnwidth}{images/teaser_depth_small.jpg}}\\
	\subfigure[Depth-adaptive superpixels]{\incgfx{0.48\columnwidth}{images/teaser_dasp_small.jpg}}
	\subfigure[Segments from sDASP]{\incgfx{0.48\columnwidth}{images/teaser_segments_small.jpg}}
	\caption{Superpixels and segments}
\end{figure}
In section \ref{sec:segmentation}, we show how spectral graph theory \cite{Shi2000} can be used on top of depth-adaptive superpixels to generate a full segmentation of the image.
Our method is general and can be used to compute segmentations from any oversegmentation.
%Evaluation results show that both methods achieve significantly better performance than state-of-the art algorithms which only use color information. To the best of our knowledge no comparable algorithm which incorporate color and depth has been proposed yet.
Our evaluation in section \ref{sec:evaluation} shows that both methods achieve significantly better performance than state-of-the art algorithms which only consider pixel color.
%To the best of our knowledge, no comparable algorithm which incorporates color and depth has been proposed yet.

\begin{figure*}
	\label{fig:density}
	\centering
	\subfigure[Image with DASP borders]{\incgfx{0.225\textwidth}{images/seed_color_small.jpg}}
	\subfigure[Superpixel density]{\incgfx{0.225\textwidth}{images/seed_density_small.jpg}}
	\subfigure[Sampled cluster seeds]{\incplt{0.246\textwidth}{images/seeds_0}}
	\subfigure[Centers after 20 iterations]{\incplt{0.246\textwidth}{images/seeds_20}}
	\caption{Color image, cluster density (left) and cluster seeds and centers (right)}
\end{figure*}

\section{Depth-Adaptive Superpixels}
\label{ref:dasp1}

%The major contribution of our approach is to provide an oversegmentation algorithm for image sensors which in addition to RGB color values also provide depth measurements of the scene.

Given depth values $\pdth:=\depth(\ppix,\ppiy) \in \mathbb{R^{+}}$ for every pixel $(\ppix,\ppiy)$, corresponding 3D points $\ppnt$ in camera coordinates are computed with the pinhole camera model:
\begin{align}
	\ppnt = \pdth \, \Big( \frac{\ppix - c_x}{f},\, \frac{\ppiy - c_y}{f},\, 1 \Big)^T \,,
\end{align}
where $f$ and $(c_x, c_y)$ are camera parameters.

Using knowledge of the scene geometry surface, the principle of superpixels can be transferred to 3D space.
We aim to compute an oversegmentation of the surface into uniformly distributed, planar patches with a fixed radius $R$.
The parameter $R$ corresponds to the minimal size of interesting features and has to be chosen depending on the application domain.

The visible part of the surface can be parametrized by the pixel grid. Thus, a 3D oversegmentation of surface points is dual to a 2D oversegmentation of pixels.
When projecting surface patches onto the image plane, they are distorted by perspective projection, corresponding to a non-uniform oversegmentation into depth-adaptive superpixels in the image space.
To guarantee equal size of surface patches, the density of superpixels has to increase with distance from the camera and inclination of the surface normal relative to view direction. To assure that surface patches are distributed uniformly, i.e. under a Poisson disk distribution, samples drawn from the density distribution need to have a spectrum with blue noise characteristics \cite{Fattal2011}.

Desirable segment borders include color, texture and geometry edges.
While image-based approaches have to rely on the fact that often, geometric edges go along with changes in color and texture, the use of the 3d point and normal corresponding to each pixel
makes it possible to respect geometry directly.
%The point normal $\pnrm$ can be computed from the depth gradient $\nabla \depth(\ppix,\ppiy)$.
%(see Eq. \ref{eq_depth_gradient}):
%\begin{align}
%	p_n(\ppix,\ppiy) =
%		\begin{pmatrix}
%			\nabla \depth(\ppix,\ppiy)\\
%			-1
%		\end{pmatrix}^0.
%\end{align}
In our case, it is sufficient to estimate the normal $\pnrm$ using the depth gradient $\nabla \depth(\ppix,\ppiy)$ computed from finite differences:
\begin{align}
	\label{eq_depth_gradient}
	\nabla \depth(\ppix,\ppiy)
		= \frac{1}{2 d_w}
		\begin{pmatrix}
			{\scriptstyle \depth(\ppix+d_p,\ppiy) - \depth(\ppix-d_p,\ppiy)}\\
			{\scriptstyle \depth(\ppix,\ppiy+d_p) - \depth(\ppix,\ppiy-d_p)}
		\end{pmatrix}
\end{align}
We choose $d_w = \frac{R}{2}$ and compute $d_p$ as the size of $d_w$ projected into the image plane (see Eq. \ref{eq_proj}).

%In the following the measured RGB color value is denoted with $\pcol$.


%Let $P_{\text{2D}} := \big\{ (\ppix,\ppiy,\pdth,\pcol) \big\}$ the set of all image pixel locations together with their measured depth and color values and $P_{\text{3D}} := \big\{ (\ppnt,\pcol) \big\}$ the set of reconstructed and colored 3D points.
%These two representations are bijective to each other, so we can freely switch representation and speak of just the set $P$. % \cong P_{\text{2D}} \cong P_{\text{3D}}$.

%Our goal is to create an oversegmentation of the 3D points $P_{\text{3D}}$ into evenly distributed and uniformly colored flat disks with a fixed radius $R$ which is given by the user.
%Formally this is a partition $S$ of points $P$, such that for each point cluster $\mathbf{s} \in S$ the following conditions are fullfilled:

%\begin{enumerate}

%\item Clusters shall be \emph{circular} with given radius $R$, i.e. the ''error area''
%\begin{align}
	%E_R(\mathbf{s}) := \sum_{p \, \in \, \mathbf{s} \Delta B_R(\mathbf{s})}{\text{surf}(p)}
%\end{align}
%shall be minimal. $\mathbf{s} \, \Delta \, B_R(\mathbf{s})$ is the symmetric difference between cluster points and points $B_R(\mathbf{s}) := \{ \ppnt \in P \,|\, \| \ppnt - \mu_u(\mathbf{s}) \| \le R \}$ within radius $R$ of the cluster center $\mu_u(\mathbf{s}) := \frac{1}{|\mathbf{s}|} \sum_{p \in \mathbf{s}}{\ppnt}$. $\text{surf}(p)$ is the surface defined by all points which gets projected into the same pixel as point $\ppnt$.

%\item Clusters shall be \emph{flat}, that is the smallest eigenvalue of the covariance matrix $\Sigma(\mathbf{s}) := \sum_{p \in \mathbf{s}}{\ppnt^T \ppnt}$ shall be minimal.

%\item All pixels in $\mathbf{s}$ shall have a \emph{similar color}:
%\begin{align}
	%\sum_{p,q \in \mathbf{s}}{d_c\big(p_c, q_c\big)} \stackrel{!}{=} \text{min}
%\end{align}
%where $d_c$ is a chosen color metric.

%\end{enumerate}

%Note that the flatness and circularity constraint imply that the segmentation preserves geometry borders and the similar-color constraint assures the preserving of texture borders.
%However, these conditions have to be weighted against each other as they can not be fullfilled at once in general.
%The DASP algorithm allows a weighting of theses three conditions.

%\fbox{Compare circular/flat segmentation}
%\fbox{against color-preserving segmentation}

%\fbox{TODO} For single-view data, the 2D segmentation problem is dual to a segmentation problem in 3D. Our main contribution is how to solve both problems at once by switching between representations and exploiting this dualism. In the two dimensional world our result is a segmentation into depth-adaptive superpixels which consider distance to the camera and projective distortions.

%\fbox{TODO} The desired requirement for circularity and a given radius can be translated into properties of superpixels by using pixel depth $\depth$ and the pixel depth gradient  $\nabla \depth$ to account for perspective projection and distortion effects.

%In the following we will use the term ''superpoint'' when speaking of clusters of 3D points and their properties. In the literature the terms ''surfel'' \cite{Pfister2000} or ''surflet'' \fbox{refs} are used for similar concepts. The choice ''superpoint'' for 3D point clusters is directly motivated from the commonly used term ''superpixel'' for clusters of 2D pixels.

%\fbox{TODO} While superpixel shapes may vary due to local variation in color and normal direction, in average the distribution of superpixel centers on the geometry surface is required to have blue-noise spectrum in order to provide a uniform coverage. As geometry is distorted through camera projection, this results in a non-uniform superpixel density in image coordinates.


%-------------------------------------------------------------------------
\section{The DASP algorithm}
\label{sec:dasp}

We proceed in three steps. First, the density of superpixel clusters in the image space is computed from the depth image. Second, we use an efficient method to sample points which will guarantee the blue-noise spectrum property.
%These two steps are crucial as they assure the correct distribution of superpixels.
Third, a clustering algorithm assigns points to superpixels and improves superpixel centers.

%\subsection{Density of depth-adaptive superpixels}

The density of superpixel seeds at pixel $(\ppix,\ppiy)$ can be computed by considering a disk with radius $R$ whose center point is at depth $\depth(\ppix,\ppiy)$ and projected into pixel $(\ppix,\ppiy)$ on the image sensor \cite{Lagae2008}.
The projected radius of this disk is computed using the trivial equation
\begin{align}
	\label{eq_proj}
	r_p(\ppix,\ppiy) = \frac{f}{\depth(\ppix,\ppiy)} R \, .
\end{align}
For surfaces parts that are not parallel to the image plane, one has to compute the perspective distortion. We locally approximate this with an affine deformation by considering the local depth gradient $\nabla \depth(\ppix,\ppiy)$.
This gives the projected area of the disk as
\begin{align}
	A_p(\ppix,\ppiy) &= \frac{r_p(\ppix,\ppiy)^2 \, \pi}{\sqrt{\big\|\nabla \depth(\ppix,\ppiy)\big\|^2 + 1}}  \,.
\end{align}
The density of superpixels is directly computed as $\rho(\ppix,\ppiy) \propto \frac{1}{A_p(\ppix,\ppiy)}$. Figure 2 shows superpixel density and sampled seeds for an example image. The bumpy pattern results from noisy depth measurements.

%The estimated density $\rho(\ppix,\ppiy)$ of superpixels at pixel $(\ppix,\ppiy)$ is computed given the number of superpoints per square meter $\rho_0$. With the desired superpoint radius $R$ and assuming disk shaped superpoints we get $\rho_0 = \frac{1}{\pi R^2}$. Alternatively $\rho_0$ could also be specified directly as a parameter.

%Using $\rho(\ppix,\ppiy) \: A_p(\ppix,\ppiy) = \rho_0 \: A_w = \rho_0 \: \pi \: r_w^2$, the density of superpixel clusters $\rho(\ppix,\ppiy)$ is computed as
%\begin{align}
%	\rho(\ppix,\ppiy) &= \rho_0 \bigg(\frac{\depth(\ppix,\ppiy)}{f}\bigg)^2 \sqrt{\big\|\nabla \depth(\ppix,\ppiy)\big\|^2 + 1}
%\end{align}

%Naturally a covering with disks is not possible, however the derivation of $\rho$ does actually not depend on the shape of the superpoint.
%Assuming a more realistic covering with hexagons we get slightly different numbers $\rho_0 = \frac{2\sqrt{3}}{9 R^2}$, which can be seen by inspection.

%Several methods exists which provide robust normals \fbox{refs} in difficult cases with borders and occlusion.
%In our case an approximative normal computation is sufficient as errors in individual points will smooth out by the grouping process.
%In order to avoid border effects the computation of the depth gradient can be enhanced by only looking to the more planar side, i.e. the side where the second derivative estimated with a third mid point is smaller.

%\subsection{Superpixel seed sampling}

Fattal \cite{Fattal2011} describes an efficient method to draw blue-noise point samples using a multi-scale sampling scheme. Initially, few points are distributed using the lowest density frequency and shifted into an optimal configuration using the Langevin method. This process is repeated iteratively for higher frequencies by using the point configuration of the previous step, splitting points if necessary and optimizing again.
%The authors demonstrate that their method is computationaly very efficient, however especially the Langevin step is time-consuming for a large number of points.
In order to achieve realtime performance, we use the basic idea of multi-scale sampling, but do not perform the Langevin step.
During the pixel clustering step, superpixel centers are automatically shifted into a cluster configuration which approximately satisfies the blue noise spectrum property in addition to flatness and color constraints (see figure 2).

%In a realtime scenario most changes in the scene affect only local parts of the image. We make use of this fact by re-using previously found cluster positions and only adding and removing clusters seeds when the cluster seed density changes.
%Adding clusters can be performed in the same way as in a full seeding. In areas where the old cluster density is too high we proceed in the same way, but instead of adding a cluster if necessary we remove the cluster nearest to the found insertion point.
%Using an iterative adding and removing of seeds while preserving clusters in unchanged image areas we can additionally decrease the number of required iterations to 1. This initially produces superpoints which are only roughly disk shaped, but who converge after some frames to nearly perfect hexagons if the geometry topology allows it.

\begin{figure*}
	\label{fig:segmentation}
	\label{fig:border}
	\begin{minipage}[b]{0.158\textwidth}
	\subfigure[T=1]{
		\includegraphics[width=0.45\textwidth]{images/clusters/cropped/clusters_i1.png}}
	\subfigure[T=3]{
		\includegraphics[width=0.45\textwidth]{images/clusters/cropped/clusters_i3.png}}\\
	\subfigure[T=50]{
		\includegraphics[width=0.45\textwidth]{images/clusters/cropped/clusters_i50.png}}
	\subfigure[Turbo]{
		\includegraphics[width=0.45\textwidth]{images/clusters/cropped/004_turbo_2000_col.png}}
	\end{minipage}
	\subfigure[DASP with 1000 superpixels]{\incgfx{0.276\textwidth}{images/segment_color_small.jpg}}
	%\incgfx{0.48}{images/segment_graph.png}
	\subfigure[sDASP contours]{\incgfx{0.276\textwidth}{images/segment_contours_small.jpg}}
	\subfigure[sDASP segment labels]{\incgfx{0.276\textwidth}{images/segment_labels_small.jpg}}
	\caption{Far Left: DASP superpixels after a varying number of iterations T (a) - (c) are compared against Turbopixels (d). Figures (e) - (g) show sDASP segmentation results.}
\end{figure*}

%-------------------------------------------------------------------------
%\subsection{Clustering}

Similar to SLIC, pixels are assigned to superpixel clusters using an iterative k-means algorithm with a customized metric.
As superpixel clusters only change locally, the search radius during a k-means iteration can be reduced to a window around the cluster center. This results in a runtime linear in the number of pixels and is likewise used in \cite{Achanta2010} and \cite{Fattal2011}.
%This approach is sufficient as we have computed excellent cluster seeds in the previous step.
%The main work left for the clustering step is to assign points to spatially near clusters and additionally fullfill the conditions of flatness and similar color.
We consider a feature space $\mathcal{F} \subset \mathbb{R}^9$ where a pixel is represented by a feature vector consisting of the point position $\ppnt$, the pixel color $\pcol$ and the point normal $\pnrm$.
A metric $d_F$ on feature vectors $\mathbf{f}=(p_v,p_c,p_n)$ and $\mathbf{f}'=(p'_v,p'_c,p'_n)$ is defined by a linear combination of metrics defined on the components.
%\begin{align}
%	d_F(\mathbf{f}_1,\mathbf{f}_2) = \lambda_{v} \, d_{v}(v_1,v_2) + \omega_{c} \, d_{c}(c_1,c_2) + \omega_{n} \, d_{n}(n_1,n_2).
%\end{align}
\begin{align}
	d_F(\mathbf{f},\mathbf{f}') := \sum_{\circ = v,c,n}{ \mu_{\circ} \, d_{\circ}(p_\circ,p'_\circ)}
\end{align}
The Euclidean distance of points is normalized with respect to the disk radius $R$: $d_{v}(p_v,p'_v) := \frac{1}{R} \|p_v-p'_v\|$. Color distance $d_{c}$ is measured by the Euclidean distance in the CIELAB color space.
%As $\Delta_{00}$ is only meaningful for small distances is is clamped at $0.1$
%\begin{align}
%	d_{c}(c_1,c_2) := \text{min}(\frac{\Delta_{00}(c_1,c_2)}{0.1}, 1)
%\end{align}
$d_{n}$ measures the angle between normals: $d_{n}(p_n,p'_n) := 1 - p_n \circ p'_n$.
%The weights $\mu_{v}$, $\mu_{c}$ and $\mu_{n}$ depend on the application.
As weights we used $\mu_{v} = 1$, $\mu_{c} = 2$ and $\mu_{n} = 3$ for all results.
%Fig. \ref{fig:border} compares superpixels after several iterations $T$ of the k-means algorithm against Turbopixels. Note that the difficult border between hand and background is respected by DASP.
%\begin{figure}[]
	%\label{fig:border}
	%\subfigure[DASP T=1]{
		%\includegraphics[width=0.23\columnwidth]{images/clusters/cropped/clusters_i1.png}}
	%\subfigure[DASP T=3]{
		%\includegraphics[width=0.23\columnwidth]{images/clusters/cropped/clusters_i3.png}}
	%\subfigure[DASP T=50]{
		%\includegraphics[width=0.23\columnwidth]{images/clusters/cropped/clusters_i50.png}}
	%\subfigure[Turbopixels]{
		%\includegraphics[width=0.23\columnwidth]{images/clusters/cropped/004_turbo_2000_col.png}}
	%\caption{Superpixel border quality}
%\end{figure}

%As normals are computed by differences and in the case of the Mircrosoft Kinect device depth information gets increasingly erroneous with distance from the camera, we also experimented with a depth adaptive normal weight $\lambda_{n}(p_\depth,p'_\depth) \propto (1 + \frac{1}{2}(p_\depth + p'_\depth))^{-1}$.


%-------------------------------------------------------------------------
\section{Image segmentation with DASP}
\label{sec:segmentation}

Superpixels provide a sparse image representation, making them suitable as a preprocessing stage in various computer vision algorithms. We demonstrate this for the case of image segmentation.
Spectral graph theory forms the basis of the normalized cuts algorithm \cite{Shi2000}, which extracts global shape information using local pixel affinity. This idea is transferred to superpixels by analyzing the superpixel neighborhood graph and considering superpixel affinity.

Let $N$ the number of superpixels and $B_{kl}$ the set of pixels on the border between two superpixels $1 \le k, l \le N$, represented by their mean feature vectors $\bar{\mathbf{f}}_k$ and $\bar{\mathbf{f}}_l$.
The superpixel affinity matrix $W \in \mathbb{R}^{N \times N}$ is constructed by using a metric similar to $d_F$:
\begin{equation}
	W_{kl} := \left\{
		\begin{array}{l l}
			e^{-d'_F(\bar{\mathbf{f}}_k, \bar{\mathbf{f}}_l)}, & B_{kl} \ne \emptyset \\
			0, & B_{kl} = \emptyset\\
		\end{array}\right. .
\end{equation}
To avoid strong disconnections at geometry edges %which may have an unstabelizing effect
we use $d'_v := \min(d_v,5)$.
In image segmentation, convex geometry edges usually do not indicate a segment border. Thus $d'_n := d_n$ if two superpixel surface patches form a concave surface and $0$ otherwise. $d'_c$ is $d_c$ scaled with the constant $\sqrt{N}/100$.
Using the diagonal matrix $D_{kk} := \sum_{l=1}^{N}{W_{kl}}$, we solve the sparse general eigenvalue problem
\begin{equation}
	(D - W) \, {\bf x} = \lambda \, D \, {\bf x} \,.
\end{equation}
%Using the superpixel graph instead of the pixel graph greatly reduces the complexity of the eigenvalue problem which has a runtime of $O(N^3)$.

We proceed in a similar way as the contour detector sPb from \cite{Arbelaez2011} to extract borders from eigenvectors $v_t$ and eigenvalues $\lambda_t$. However, we only need to consider pixels on borders between superpixels, which greatly simplifies the process. The border intensity image is computed as
\begin{equation}
	I(i,j) := \sum_{t=1}^{C}{\frac{1}{\sqrt{\lambda_t}} |v_{tk} - v_{tl}|}, \; (i,j) \in B_{kl} \,.
\end{equation}

Segment contours are extracted by using a threshold on $I$. Segment labels are received by computing connected components on the superpixel graph reduced to edges below the threshold (see figure \ref{fig:segmentation}).
This method can be applied to any oversegmentation algorithm like Turbopixels or SLIC (named sSLIC in figure 4).

%An ultrametric contour map \cite{Arbelaez2006} can be used to directly construct segment labels from the contour image. This method is gen

%-------------------------------------------------------------------------
\section{Evaluation}
\label{sec:evaluation}

We compare our algorithm against state-of-the-art using a RGB-D dataset consisting of 11 images which were captured with the Microsoft Kinect sensor.
Ground truth labels were generated manually.
%The dataset is available on our homepage \fbox{link}.

The quality of an oversegmentation is generally measured through boundary recall and undersegmentation error \cite{Levinshtein2009}. The results in figure 4 show that DASP outperforms Turbopixels and SLIC with respect to boundary recall. The undersegmentation error is bigger for DASP due to noisy depth values near geometry edges.

\begin{figure*}[tbp]
	\label{fig:eval}
	\centering
	%\subfigure[Boundary Recall]{
	\incplt{0.33\textwidth}{images/dasp_recall}
	%\subfigure[Undersegmentation Error]{
	\incplt{0.33\textwidth}{images/dasp_use}
	%\subfigure[Thickness of DASP Superpixel]{\incplt{0.24}{images/dasp_thickness}}
	%\subfigure[Contour Detection]{
	\incplt{0.33\textwidth}{images/dasp_segments}
	%\subfigure[Segment Coverage]{\incplt{0.24}{images/dasp_segments}}
	%\subfigure[Computation time]{\incplt{0.24}{images/dasp_runtime}}
	%\subfigure[Thickness of DASP Superpixel]{\incplt{0.24}{images/dasp_thickness}}
	\caption{Boundary recall (left) and undersegmentation error (middle) for oversegmentation algorithms and contour detector quality (right) are compared on our dataset.}
\end{figure*}

To evaluate segmentation quality we compare our algorithm against the algorithm gPb-owt-ucm from \cite{Arbelaez2011} under the measure of segment boundary quality. gPb-owt-ucm ranks highest on the Berkeley Segmentation Dataset (BSDS300) Benchmark \cite{Martin2001} with a maximal F-score of 0.71.
On our dataset, gPb-owt-ucm achieves a maximal F-score of 0.70, while sDASP scores 0.76. Figure 4 shows the precision/recall graph of gPb-owt-ucm and sDASP and sSLIC with 1000 superpixels.

In table 1 we report execution times for a 2.53 GHz single-core CPU. Our algorithms outperforms Turbopixels and gPb-owt-ucm by several orders of magnitudes.
All results are with 20 k-means clustering iterations. Reducing the number of iterations to 3 results in a runtime of ca. 10 fps with slightly reduced quality.

%\begin{table}[h]
	%\label{tab:runtime}
	%\centering
	%\begin{tabular}{c|c|c|c|}
		%\cline{2-4}
		%& n=200 & n=1000 & n=2000\\
		%\hline
		%\multicolumn{1}{|c|}{Turbopixels} & 16.5 & 13.6 & 13.8\\
		%\multicolumn{1}{|c|}{DASP} & 0.53 & 0.58 & 0.62\\
		%\multicolumn{1}{|c|}{sDASP} & 0.6 & 1.2 & 4.7\\
		%\cline{2-4}
		%\multicolumn{1}{|c|}{gPb-owt-ucm} & \multicolumn{3}{|c|}{217.8}\\
		%\hline
	%\end{tabular}
%\end{table}

%\footnotetext{The dataset and further results are available on our homepage under http://ias.in.tum.de/~weikersd/dasp.html}

\begin{table}[h]
	\label{tab:runtime}
	\centering
	%\footnotesize
	\begin{tabular}{l|c|c||c|c|}
		\cline{2-5}
		& Turbo & DASP & gPb & sDASP\\
		\hline
		\multicolumn{1}{|l|}{n=200} & 16.5 s & {\bf 0.53 s} & \multirow{3}{*}{218 s} & {\bf 0.54 s}\\
		\multicolumn{1}{|l|}{n=1000} & 13.6 s & {\bf 0.58 s} & & {\bf 1.64 s} \\
		\multicolumn{1}{|l|}{n=2000} & 13.8 s & {\bf 0.62 s} & & {\bf 7.36 s} \\
		\hline
	\end{tabular}
	\caption{Comparison of algorithm runtime}
\end{table}

%Using runtime optimizations, DASP and sDASP enables the usage of sophisticated state-of-the art image segmentation algorithms in real time applications.

%\begin{figure}[!tp]
%	\centering
%	\label{fig:eval_pr}
%	\caption{Segment Contour Detection}
%	\includegraphics[width=0.90\columnwidth]{images/dasp_segments}
%\end{figure}

\section{Conclusions}

DASP is a novel oversegmentation algorithm for RGB-D images. % captured by sensors like the Microsoft Kinect.
In contrast to previous approaches it uses 3D information in addition to color.
%Our oversegmentation and segmentation algorithms outperform state-of-the-art 2D algorithms which use only color both in quality and speed.
%Depth-adaptive superpixels provide a dense image representation and can be the starting point for various applications, for example a full segmentation of the image.
With depth-adaptive superpixels, previously costly segmentation methods are available to realtime scenarios, opening a variety of new applications.
We demonstrate this by using spectral graph theory for an image segmentation which outperforms state-of-the-art algorithms both in quality and speed.

The source code and data set used for this paper are available at {\emph {https://github.com/Danvil/dasp}}.

%\balance

%-------------------------------------------------------------------------
%\nocite{ex1,ex2}
\bibliographystyle{latex12}
\bibliography{danvil}

%-------------------------------------------------------------------------
\if 0
Superpixels
\begin{itemize}
\item Superpixels \cite{Ren2003}
\item NCuts \cite{Shi2000}
\item Turbopixels \cite{Levinshtein2009}
\item SLIC Superpixels \cite{Achanta2010}
\end{itemize}
Applications
\begin{itemize}
\item Superpixel Tracking \cite{Wang2011}
\item Class Segmentation with Superpixel Neighborhoods \cite{Fulkerson2007}
\item UCM \cite{Arbelaez2006}
\end{itemize}
Misc
\begin{itemize}
\item Surfel \cite{Pfister2000}
\item Blue Noise \cite{Fattal2011}
\item Mean Shift \cite{Comaniciu2002}
\item Quick Shift \cite{Vedaldi}
\end{itemize}
\fi
%-------------------------------------------------------------------------

\end{document}

