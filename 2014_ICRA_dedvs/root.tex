%!TEX root = root.tex

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

\documentclass[letterpaper, 10 pt, conference]{ieeeconf}
% Comment this line out if you need a4paper

%\documentclass[a4paper, 10pt, conference]{ieeeconf}
% Use this line for a4 paper

\IEEEoverridecommandlockouts
% This command is only needed if you want to use the \thanks command

\overrideIEEEmargins
% Needed to meet printer requirements.

% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

% The following packages can be found on http:\\www.ctan.org
%\usepackage{graphics} % for pdf, bitmapped graphics files
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{times} % assumes new font selection scheme installed
%\usepackage{amsmath} % assumes amsmath package installed
%\usepackage{amssymb}  % assumes amsmath package installed

\title{\LARGE \bf
Event-based 3D SLAM with a depth-augmented dynamic vision sensor
}


\author{
	David Weikersdorfer$^{1}$, David B. Adrian$^{1}$, Daniel Cremers$^{2}$ and J\"org Conradt$^{1}$%
	\thanks{*This work was not supported by any organization}%
	\thanks{$^{1}$
		Neuroscientific System Theory,
		Institute of Automatic Control Engineering,
		Technische Universit\"at M\"unchen,
		Munich, Germany
	}%
	\thanks{$^{2}$
		Computer Vision Group,
		Computer Science Department,
		Technische Universit\"at M\"unchen,
		Munich, Germany
	}%
}

\usepackage{amsmath,amssymb,amsfonts,bm}

\usepackage{graphicx}
\graphicspath{
	{./images/}
	%{/home/david/Documents/Science/papers/2014_icra_dedvs/}
	%{/home/dadrian/wuala/WualaDrive/Contacts/Danvil/Science/papers/2014_icra_dedvs/}
}

\usepackage{booktabs}
\usepackage{array}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\usepackage{multirow}

\usepackage{xspace}

\newcommand{\reffig}[1]{fig.~\ref{fig:#1}}
\newcommand{\Reffig}[1]{Fig.~\ref{fig:#1}}
\newcommand{\refeq}[1]{eq.~\ref{eq:#1}}
\newcommand{\Refeq}[1]{Eq.~\ref{eq:#1}}
\newcommand{\reftab}[1]{table~\ref{tab:#1}}
\newcommand{\Reftab}[1]{Table~\ref{tab:#1}}
\newcommand{\refalg}[1]{alg.~\ref{alg:#1}}
\newcommand{\Refalg}[1]{Alg.~\ref{alg:#1}}
\newcommand{\refdef}[1]{def.~\ref{def:#1}}
\newcommand{\Refdef}[1]{Def.~\ref{def:#1}}
\newcommand{\refsec}[1]{section \ref{sec:#1}}
\newcommand{\Refsec}[1]{Section \ref{sec:#1}}

\newcommand{\edvsK}{\emph{eDVS}\xspace}
\newcommand{\dedvsK}{\emph{D-eDVS}\xspace}
\newcommand{\edvs}{\emph{Embedded Dynamic Vision Sensor}\xspace}
\newcommand{\ebslamK}{\emph{EB-SLAM-3D}\xspace}

\newcommand{\reals}{\mathbb{R}}
\newcommand{\realspos}{\mathbb{R}_{+}}
\newcommand{\seg}[1]{\text{SE}(#1)}
\newcommand{\sog}[1]{\text{SO}(#1)}

\newcommand{\ddm}{\mathcal{M}}
\newcommand{\ddr}{\mathcal{R}}
\newcommand{\ddP}{\mathcal{P}}

\newcommand{\vectornorm}[1]{\left\|#1\right\|}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LINKS
% http://ras.papercept.net/conferences/support/tex.php
% http://web.utk.edu/~jtan10/icra2014/


\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}

We present the \dedvsK -- a combined event-based 3D sensor -- and a novel event-based full-3D simultaneous localization and mapping algorithm which works exclusively with the sparse stream of visual data provided by the \dedvsK.
The \dedvsK is a combination of the established PrimeSense RGB-D sensor and a biologically inspired embedded dynamic vision sensor.
Dynamic vision sensors only react to dynamic contrast changes and output data in form of a sparse stream of events which represent individual pixel locations.
We demonstrate how an event-based dynamic vision sensor can be fused with a classic frame-based RGB-D sensor to produce a sparse stream of depth-augmented 3D points.
The advantages of a sparse, event-based stream are a much smaller amount of generated data, thus more efficient resource usage, and a continuous representation of motion allowing lag-free tracking.
Our event-based SLAM algorithm is highly efficient and runs 20 times faster than realtime, provides localization updates at several hundred Hertz, and produces excellent results.
We compare our method against ground truth from an external tracking system and two state-of-the-art algorithms on a new dataset which we release in combination with this paper.

\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}

Simultaneous localization and mapping (SLAM) is one of the central tasks in robotics and computer vision which enables robots to explore and operate in unknown and unconstrained environments.
While 2D or 2.5D SLAM which creates a bird-view map is an well-addressed problem \cite{doucet2000rao,grisetti2007improved,montemerlo2002fastslam}, the full-3D SLAM problem has been tackled in recent years with the aid of combined color and depth (RGB-D) sensors like the PrimeSense device \cite{shotton2011bodypartrecognition}.
% Such sensor are active and provide depth measurements for every pixel with the help of a projected infrared pattern and thus elegantly circumferences the multitude of problems occurring with depth from stereo.
An important example for a 3D SLAM algorithm is KinectFusion \cite{newcombe2011kinectfusion}, a dense 3D SLAM algorithm which uses iterative closest point to match depth images and a signed distance volume as a 3D map.
The method of Bylow et al. \cite{bylow2013rss} improves on KinectFusion by using a more sophisticated representation of the signed distance function and a better optimization scheme.
Kerl et al. \cite{kerl13iros} presented another dense visual SLAM method which uses the photometric and depth error to optimize the current position estimate.
However many current dense 3D SLAM methods have the crucial disadvantage that they are very resource intensive and algorithms require dedicated GPU hardware which is expensive and has a high power consumption.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.8\columnwidth]{images/intro.png}
	\caption{
		Our event-based SLAM method \ebslamK creates a sparse 3D map (gray) which captures salient features like edges.
		The map is incrementally build using only 3D point events (green) provided by an dynamic vision sensor where each pixel event is augmented with depth information from an RGB-D camera.
		Due to properties of the dynamic vision sensor, \ebslamK creates excellent results for self-localization (red/orange). 
	}
	\label{fig:intro}
\end{figure}

\begin{figure*}[th!]
	\centering
	\includegraphics[width=\textwidth]{images/stream.jpg}
	\caption{
		Top: The PrimeSense color data stream for references.
		Middle tpp: The depth stream of the PrimeSense device with depth encoded as color (red to blue indicates near to far).
		Middle bottom: The event-stream provided by the \edvsK sensor. Dark points indicate pixels which have fired an event.
		Bottom: The fused depth and event stream produces a sparse event-stream with depth information.
	}
	\label{fig:stream}
\end{figure*}

We use the low-cost embedded dynamic vision sensor (\edvsK) \cite{conradt2009} to intelligently reduce the amount of data which needs to be processed for tracking and mapping.
Each pixel of the \edvsK individually and asynchronously detects changes in the perceived illumination and fires pixel location events when the change exceeds a certain threshold.
Thus events are mainly generated at salient image features like edges which are for example due to geometry or texture edges.
In this sense we choose a middle ground between using only singular feature points and using all pixels.
Depth information is an important requirement for 3D SLAM and as the \edvsK is not a depth-sensor we will first augment pixel events with depth information by combining the \edvsK with a separate, active depth-sensing camera like the PrimseSense sensor.
This results in a sparse stream of 3D point events in camera coordinates which directly give the 3D position of salient edges in the 3D scene. %ANM: edges -> features

The paradigm of event-based vision requires the development of new methods and we propose a novel event-based 3D SLAM algorithm (\ebslamK) which works solely on the sparse point event stream.
Our algorithm uses a modified particle filter for tracking the current position and orientation of the camera while at the same time incrementally creating a map of the previously unknown environment.
We update the internal belief state about location and map for every event and are thus able to provide pose estimates with very low latency.
At the same time the computational task carried out for every event has minimal complexity allowing very fast event processing.
Low latency and low computational overhead are both important requirements for tight control loops found in quad-copters, or for autonomous vehicles which need to react quickly to changes in the environment.

This paper continues with a short introduction into event-based vision in \refsec{ebv} and in \refsec{dedvs} the concept and calibration of the \dedvsK sensor is described.
In \refsec{ebslam} the event-based 3D SLAM algorithm is presented and tracking and map generation are explained in detail.
The paper is concluded with a thorough evaluation of the \ebslamK algorithm in \refsec{eval} and a conclusion in \refsec{conclusions}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{EVENT-BASED VISION}
\label{sec:ebv}

Conventional camera sensors provide recorded data as entire images at a fixed frequency, e.g. 30 or 60 Hz, regardless if the image has changed since the last frame.
This results in a semi-continuous stream of highly redundant data which requires very high bandwidth.
Furthermore they suffer from a fixed exposure time, resulting in a limited dynamic range and increasing the difficulty for image processing in unsteady light conditions.

Dynamic vision sensors \cite{lichtsteiner2008dvs} like the \edvsK \cite{conradt2009} are a novel type of gray-scale image sensors that completely abandons the concept of frame-based vision.
All pixels of the \edvsK operate asynchronously and individually integrate the measured light intensity over time.
When the accumulated change is higher than a certain threshold an event is fired and the baseline is reset.
An event consists of the pixel location $u \in \ddr$ on the sensor, in our case it has a resolution of $128\times128$ pixel, and a timestamp with micro-second accuracy indicating the time of occurrence of the event.
Additionally a parity bit is reported which indicates if illumination has increase or decrease.

When an event is triggered the corresponding pixel location and the current time are inserted as an event into the event stream.
Only if the change in illumination for a pixel is high enough, the generation of an event will be triggered.
Typical image locations where events are generated are edges or other salient features with a high local contrast.
Events only occur for moving entities in the scene or upon ego-motion of the sensor and for a static scene and motionless sensor no events, except noise events, are generated and no data will be transmitted.
An important property of dynamic vision sensors is the fact that the number of generated events only depend on the amount of moved distance and not on the velocity.
The only difference is that faster motions generate a higher number of events per seconds as the same distance is traversed in a smaller time.
This counteracts problematic blurring in classic frame-based sensors, as all events necessary to understand a motion are detected and reported.
This is a very beneficial property for example for accurate high-speed tracking.
It is of course only true up to a certain limit, but dynamic vision sensors are capable of producing up to one million events per second which is enough for very fast motions.
A conventional frame-based sensor would need to operate with several hundred frames per second to produce similar results.
As a summary, the \edvsK directly creates a sparse stream of dynamic changes in hardware requiring no further software preprocessing to remove redundant information.

\Reffig{stream} shows an example of a stream of events generated by an \edvsK in comparison to a classic frame-based camera like the PrimeSense RGB-D sensor.
For visualization and printing several events are displayed together in one frame as if they have occurred at the same time, while in reality they occur one after the other and form a continuous stream of pixel events.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{THE D-EDVS SENSOR}
\label{sec:dedvs}

Raw events from an \edvsK are only projected pixel locations which lack information about the exact world coordinate of the point which generated the event.
The task of augmenting events with their respective depth is a non-trivial problem.
One approach to acquire depth information is a fully event-based stereo vision set-up consisting of two \edvsK sensors.
While such a solution has been successfully demonstrated \cite{rogister2012asynchronous}, the capabilities are rather limited and not sufficient for deploying a 3D SLAM method.
As a consequence we chose to provide depth information by a dedicated depth sensor like the PrimeSense RGB-D sensor.
Our implementation combines an \edvsK with an Asus Xtion Pro Live (see \reffig{dedvs}).
This implies a compromise between a frame-based and an event-based approach as depth information is not available with the same speed as events are generated.
However event depth values can be interpolated between depth frames which gives good results for many cases.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.475\columnwidth]{images/dedvs1.jpg}
	\includegraphics[width=0.42\columnwidth]{images/dedvs2.jpg}
	\caption{
		The \dedvsK sensor (left) is a combination of an Asus Xtion {RGB-D} sensor (black casing) and a \edvsK (white casing).
		The \edvsK (right) is displayed with various accessories like WiFi module and lens mount, and a coin for size comparison.
	}
	\label{fig:dedvs}
\end{figure}

The main task of fusing the information of the \edvsK and PrimeSense is the calibration of pixel correspondences, as we need to find the correct depth value for each event pixel location.
In the following both cameras will be modeled as regular pinhole cameras $\mathrm{K}(f,c)$ where $f$ is the focal length and $c \in \mathbb{R}^2$ the center of projection.
The mapping of world points $x \in \mathbb{R}^3$ to image points $u \in \mathbb{R}^2$ is defined as $u = \mathrm{K} \, \mathrm{T} \, x$ where $\mathrm{T}$ is a transformation matrix representing a rotation and a translation.
For real lenses it is necessary to also account for radial distortion.
A simple distortion model like
\begin{equation}
L(u) = u \, (1 + \kappa_1 r + \kappa_2 r^2) \,, \;\; r=\vectornorm{u}
\end{equation}
with $\kappa_1, \kappa_2$ distortion parameters is sufficient for the \edvsK and in case of the PrimeSense sensor the distortion is already compensated well enough in hardware.
% In the following we assume that image coordinates for the \edvsK are distorted correctly.

Normally it is only possible to back-project image points to rays, but if depth information is available the inverse of the projection is well-defined.
Thus in case of the RGB-D sensor, we can compute $x = \mathrm{T}^{-1} \mathrm{K}_d^{-1} \, u_d$ of RGB-D image points $u_d$ to world points where $K_d = K(f_d,c_d)$.
Subsequently we can compute corresponding \edvsK image coordinates $u_e$ as 
\begin{equation}
	u_e = L^{-1}(\mathrm{K}_e \, \mathrm{T} \, \mathrm{K}_d^{-1} \, u_d) \,.
\end{equation}
This allows to establish a relation between the RGB-D sensor and corresponding pixel locations on the event-based sensor.

To compute the internal camera parameters for $\mathrm{K}_e$ and $\mathrm{K}_d$ and the relative transformation matrix $\mathrm{T}$, we record a set of corresponding pixel locations on both sensors and find the minimum of the least-square problem
\begin{equation}
	\textrm{argmin} \sum_{i=1}^{N}{\left\| u_{e,i} - L^{-1}(\mathrm{K}_e \, \mathrm{T} \, \mathrm{K}_d^{-1} \, u_{k,i}) \right\|^2}
	\label{eq:calib_lqp}
\end{equation}
for the parameters $f_e$, $c_e$, $\kappa_1$, $\kappa_2$ and rotation and translation of $\mathrm{T}$.
The internal parameters of the PrimeSense device are known and do not need to be optimized.
For our problem we solve \refeq{calib_lqp} by using local optimization as good initial values are available.

To find the necessary pixel correspondences we use a diode which emits pulses of light with a fixed and known frequency.
This allows easy detection the position of the diode in the event-based data stream by using frequency filtering.
We reject all events at pixel locations where the time since the last event does not match the pulsing frequency (see \reffig{calib}).
On the other hand, the position of the diode needs to be detected in the depth image of the RGB-D camera.
Here we use built-in registration between color and depth images to be able to work with the color image instead of the depth image.
We use the OpenCV library to detect a checkerboard in which the diode has been placed in the middle of the board (see \reffig{calib}).

\begin{figure}[b!]
	\centering
	\includegraphics[trim=0 50px 0 100px, clip=true, width=0.9\columnwidth]{images/calib.jpg}
	\caption{
		To get corresponding points for camera calibration we track the position of a pulsing diode in both data streams by detecting a checkerboard in the color image (left) and by using a frequency filter on events (right).
		Only events which occur with the pulsing frequency of 100 Hz of our diode are used for the position estimate (blue) and other events (red) are rejected.
	}
	\label{fig:calib}
\end{figure}

With the calibrated camera model it is possible to transform every pixel of the depth image to the corresponding pixel location in the event-based image.
However to augment events with depth information the inverse operation is required which is a conceptually more difficult problem.
We solve it by updating a $128\times128$ pixel depth-map for every new depth-frame which represents the current mapped depth values of the scene as seen by the \edvsK sensor.
Now for each new event a simple look-up in the depth map gives the desired depth value.
Additionally one can perform temporal interpolation between subsequent depth maps to increase the accuracy of depth values for events occurring between the frames of the RGB-D sensor.

The bottom row in \reffig{stream} shows an example of a depth augmented event stream.
This stream will be the only input used for the proposed event-based 3D SLAM -- the dense depth or color image is not used any further.

\begin{figure*}[th!]
	\centering
	\includegraphics[width=0.268\textwidth]{images/map_eb.jpg}\hspace{0.6cm}
	\includegraphics[width=0.268\textwidth]{images/map_mesh.jpg}\hspace{0.35cm}
	\includegraphics[width=0.268\textwidth]{images/map_color.jpg}
	\caption{
		Left: The event-based 3D map $\ddm$ used by our algorithm for tracking.
		Black color indicates a high probability for event-generation and thus a high score for particle evaluation.
		Middle: A mesh generated from the voxel-based map used by KinFu for comparison.
		Right: Color image for reference.
	}
	\label{fig:map}
\end{figure*}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{EVENT-BASED 3D SLAM}
\label{sec:ebslam}

As the mathematical foundation for event-based 3D SLAM we use a dynamic Bayesian network and the Condensation particle filter algorithm \cite{isard1998condensation}.
In the dynamic Bayesian network used by the Condensation algorithm the current system state is modeled as a temporal random variable $X_k$ which is inferred using only the current measurement $Z_k$.
The probabilistic density is realized by a set of $N$ particles $(p_i,s_i)$ where each represents a possible system state $p_i$ together with a ''score'' $s_i \in \realspos$ indicating how well it represents the latest observed measurement.
Particles for the next frame are selected based on this score, this step is called ''resampling'', and the procedure is iterated.
In our case the system state is the special Euclidean group representing the current pose $p_i = (t_i,q_i) \in \seg{3}$ of the camera, i.e. its position $t_i \in \reals^3$ and its orientation $q_i \in \sog{3}$.
Normally the Markov assumption is used which states that only the last measurement $Z_k$ has to be considered instead of the whole history of measurements $\mathcal{Z}_k = (Z_1,\ldots,Z_k)$.
This leads to:
\begin{equation}
	\small P(X_k | \mathcal{Z}_k) \propto P(Z_k | X_k) \int P(X_k | X_{t-1}) \, P(X_{t-1} | \mathcal{Z}_{t-1}) \, \mathrm{d} X_{t-1}
\end{equation}
Here the sensor model $P(Z_k | X_k)$ defines the probability that a given state explains the current measurement.
The motion model $P(X_k | X_{t-1})$ describes the dynamic change in the system and for this work no additional sensors are used, thus the motion model is simply a random diffusion process:
\begin{equation}
	P(X_k \,|\, X_{t-1} = p_i) := \mathcal{N}(p_i,\Sigma)
\end{equation}

The Markov assumption is reasonable for frame-based cameras as normally a complete image provides enough evidence to give good scores to particles.
For the event-based case individual events are highly ambiguous and do not carry enough information to rate particles.
Additionally the resampling operation is computationally expensive when executed several thousand times per second.
For these reasons an incremental model is chosen where the Markov assumption is released and the score of particles does not only depend on the current measurement but also on the recent past of measurements \cite{weikersdorfer2012edvs,weikersdorfer2013edvsslam}.
For each new event $e_k$ particle scores are updated using an exponential decay model:
\begin{equation}
	s_i = (1-\alpha) \, s_i + \alpha \, P(Z_k = e_k | X_k = p_i)
	\label{eq:s}
\end{equation}
where the decay constant $\alpha$ defines the influence of the current event compared to past events.
An intuitive derivation of $\alpha$ is found with $\alpha = 1 - (1 - \beta)^{^1/_K}$ which gives a decay constant where the last $K$ events have an influence of ${\beta \in \,\, [0,1]}$ percent on the total score $s_i$.
This change with respect to a classical particle filter matches the event-based nature of the data stream and adds only minimal computational overhead.

As an additional measure to reduce the runtime of the algorithm the resampling step is not executed for every event but only after every $K$-th event.
This is reasonable as individual events carry only little information and thus the probability density changes only little for only one event.

\begin{figure*}
	\centering
	\includegraphics[width=0.25\textwidth]{eval_numParticles.pdf}%
	\includegraphics[width=0.25\textwidth]{eval_eventCut.pdf}%
	\includegraphics[width=0.25\textwidth]{eval_mapCellSize.pdf}%
	\includegraphics[width=0.25\textwidth]{eval_resampleK.pdf}
	\caption{
		Variation of parameters for our method: Number of particles (left), percentage of used events (middle left), voxel size (middle right)  and number of events until resampling (right).
		The top row shows the RMSE and the lower row the runtime as a factor of realtime processing speed -- a factor of 2 indicates our method runs two times faster than realtime.
		Boxes show the range of values for the mid two quartiles of takes in the dataset, the black bar shows the median value, whiskers have an interquartile range of 1 and individual dots show outliers.
	}
	\label{fig:eval}
\end{figure*}

The map $\ddm : \mathbb{Z}^3 \rightarrow \realspos$ is modeled as a discretized probabilistic sparse voxel grid.
It is updated alternately with the path, a concept known as Rao-Blackwellization \cite{doucet2000rao,grisetti2007improved}.
For event-based 3D SLAM each voxel in the map should indicate the probability with which this point would generate an event if the camera moves over it.
This allows the formulation of the sensor model as
\begin{equation}
	P(Z_k = e_k \,|\, X_k = p_i) \propto \ddm(\lfloor \tfrac{1}{\lambda}(t_i + q_i \, e_k) \rceil)
\end{equation}
where $\lfloor x \rceil$ indicates the components of $x$ rounded towards the nearest integer.
The constant $\lambda \in \realspos$ is the size of voxel in world coordinates and a standard value is 0.01 m.
The number of events generated at a specific spot depends on various factors like the amount of contrast towards the background for geometry edges, the relative change in intensity for planar image features or the orientation of a feature relative to the motion direction of the camera.
However a model where each voxel counts the number of events observed at this location has proved to be sufficient.
This gives a strikingly simple iterative map update rule for every new event:
\begin{equation}
	\ddm(\lfloor \tfrac{1}{\lambda}(t^{*} + q^{*} \, e_k) \rceil) \, {+}{=} \, 1
\end{equation}
where $(t^{*},q^{*})$ is the pose of the current best particle.
Other modes to update the map are possible, like using the best particles weighted by their score or even using all particles. % but in practice prove to be less efficient and give poorer results.
\Reffig{map} shows an example of the event-based map generated by our algorithm.
It can be seen how our map only captures salient features like geometry edges or texture features and ignores uniform solid surfaces compared to a solid mesh generated by other algorithms which represents all surface information.

\newcolumntype{R}{>{\raggedleft\arraybackslash}m{1.1cm}}
\newcolumntype{M}{>{\raggedleft\arraybackslash}m{1.7cm}}

A closer analysis of the runtime of our algorithm shows that a large share of the computation time is spent with particle diffusion in the motion model.
Especially the rotation diffusion process requires the sampling of a uniformly distributed point on a sphere for the rotation axis and a normally distributed rotation angle.
To counteract we use the well known fact that two normal distributions can be added by adding their variances.
Thus we collect events in mini-batches of $B$ events, e.g. $B=3$, and treat them as one package of information.
This allows to execute normal diffusion only once per mini-batch with a standard deviation multiplied by $\sqrt{B}$.
Additionally a modification of the score update rule from \refeq{s} is required as the score update processes $B$ events at once:
\begin{equation}
	s_i = (1-\alpha)^B \, s_i + \frac{1-(1-\alpha)^B}{B} \, \sum_{j=0}^{B-1}{P(e_{k+j} \,|\, p_i)}
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{EVALUATION}
\label{sec:eval}

% \begin{figure}
% 	\centering
% 	\includegraphics{series/series.jpg}
% 	\caption{
% 		series
% 	}
% 	\label{fig:series}
% \end{figure}

For evaluation we compare our algorithm against ground truth and against two state-of-the-art algorithms: Kinect Fusion \cite{newcombe2011kinectfusion} and Kerl et al. \cite{kerl13iros}.
As we require event-based data we recorded a new dataset consisting of five different scenarios and a total of 26 takes, where each take is one recording of length 20 to 40 seconds.
Ground truth data for our dataset is provided by the marker-based motion tracking system OptiTrack V100:R2.
Our dataset is released to the public as part of this paper and consists of the event stream, ground truth data and full color and depth streams from the PrimeSense device for evaluation with other algorithms.
For a comparison against KinectFusion we use the open source KinFu implementation of the Point Cloud Library (PCL) \cite{web:kinfu} and for Kerl et al. we use their publicly available open source implementation.
A comparison of the root-mean-square error (RMSE) of \ebslamK, KinectFusion and Kerl et al. compared to ground truth is shown in \reftab{rmse}.
We attribute a crucial contribution of our low RMSE to the event-based representation of motion which does not, like KinFu or Kerl et al., suffer from motion blur or too fast or non-constant velocities.
In scenario 4 the sensor was moved around a large room which is a setting prone to loop-closure issues.  
The relatively high RMSE in scenario 4 shows that our algorithm can not fully handle this as no global path optimizer is used.

\begin{table*}[t!]
	\caption{Evaluation of \ebslamK, KinFu and Kerl et al.
	% 	Positional and rotational root-mean-square error (RMSE), number of events per second and percent of data processable for realtime execution for the scenarios in the dataset.
	}
	\centering
	\begin{tabular}{lc|M|RR|RR|Rc|RR}
		\toprule
		\multirow{2}{*}{Scenario} & \multirow{2}{*}{Takes} & \ebslamK & \multicolumn{2}{c|}{\ebslamK (quality)} & \multicolumn{2}{c|}{\ebslamK (speed)} & \multicolumn{2}{c|}{KinFu \cite{web:kinfu}} & \multicolumn{2}{c}{Kerl et al. \cite{kerl13iros}} \\
		 & & RAM & RMSE & Speed & RMSE & Speed & RMSE & Failures & RMSE & Speed \\
		\midrule
		1: ''Table 1''   & 2 & 25 MB &  3.1 cm & 2.0 x &  4.0 cm & 20 x &     N/A & 2 &  7.7 cm & 0.23 x\\
		2: ''Sideboard'' & 4 & 14 MB &  4.0 cm & 2.2 x &  5.2 cm & 23 x &  4.3 cm & 1 &  7.2 cm & 0.23 x\\
		3: ''Table 2''   & 8 & 27 MB &  4.9 cm & 1.4 x &  9.1 cm & 16 x & 16.5 cm & 3 &  8.9 cm & 0.28 x\\
		4: ''Room''      & 8 & 21 MB & 13.4 cm & 2.5 x & 13.3 cm & 27 x & 28.8 cm & 6 & 12.2 cm & 0.23 x\\
		5: ''People''    & 4 & 15 MB &  6.1 cm & 2.2 x &  7.0 cm & 24 x & 12.4 cm & 3 &  4.5 cm & 0.24 x\\
		\bottomrule
	\end{tabular}
	\label{tab:rmse}
\end{table*}

We report results for our algorithm with two parameter sets: A ''quality'' configuration which provides a low RMSE and is running faster than realtime, and a ''speed'' configuration which yields a slightly higher RMSE but runs more than 20 times faster than required for realtime processing.
We evaluate runtime in form of a ''realtime runtime factor'' which is the quotient of the total time duration of the corresponding take and the time required for processing all occurred events.
This is necessary as the event-rate is not constant and the computation time thus depends on the velocity of the sensor.  
All runtime durations for \ebslamK and Kerl et al. are measured on a single-core Intel i7 1.9 GHz CPU which consumes only 17 Watts of power and the GPU required by KinFu is a Nvidia GTX 670.
Even in the ''speed'' setting our algorithm gives very good results which demonstrates the excellent performance of \ebslamK.

Additionally one can consider a comparison of power consumption: The graphics card used for KinFu requires 170 Watts of power and can barely process the data in realtime, while our algorithm runs 20 times faster than realtime with only 17 Watts of power, effectively consuming only about 1 Watt.
In terms of memory requirements, the map has by far the biggest influence and requires around 12 to 30 MB of RAM depending on scenario and path.
Thus, our sparse voxel grid requires significantly less memory than a dense voxelgrid which would require 64 MB for volume with side length $256$ voxels and 512 MB for $512^3$ voxels.
Low memory requirements, high computational efficiency and low power consumption make \ebslamK an excellent candidate for autonomous robots or even embedded systems.

\addtolength{\textheight}{-3.0cm} 

Additionally we analyze the influence of several parameters of \ebslamK on quality and runtime performance.
\Reffig{eval} shows the RMSE and the realtime runtime factor for the following parameters: Number of particles $N$, percentage of events used, voxel size $\lambda$ and number of events until resampling $K$.
The number of particles used in the particle filter have a direct linear influence on the runtime of the algorithm and our algorithm operates reliable even with low numbers of up to 40 particles.
By varying the percentage of events a certain share of events is completely ignored and theses events are neither used for score update nor for map update.
Reducing the number of used events makes the algorithm more unstable, but at the same time faster as no processing is required for ignored events.
Evaluation shows that our algorithm works reliable with up to only 50$\%$ of events which inturn results in half the processing time.
The voxel size defines the coarseness of our voxel grid and indicates the size length of a cubic voxel.
The voxel size has mainly an influence on the memory requirements and reported memory values in \reftab{rmse} are for $\lambda=0.02$, the default value used in the ''fast'' parameter set.
The last analyzed parameter in \reffig{eval} is $K$, the number of events until resampling.
A value of 1 corresponds to a classical particle filter and evaluation shows that this is not a good choice for event-based sensors.
This justifies the usage of the exponential decay model in \refeq{s}.
Particle selection needs to consider at least several events for successful tracking and low runtime.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{CONCLUSIONS}
\label{sec:conclusions}

The combination of dynamic vision sensors and RGB-D sensors is a promising opportunity for a new type of visual processing.
The pre-processing provided by the hardware generates a continuous, sparse stream of 3D points events which captures only dynamic and salient information.
The proposed event-based 3D SLAM algorithm is a strikingly simple algorithm which can produce excellent results twenty times faster than realtime without using special hardware like a GPU.
Our algorithm is therefore especially suitable for small robots, flying robots and high-speed applications.
As possible for future work we see the usage of a global optimizer to handle loop closure problems, and the augmentation of our map with sparse pieces of surface information to better predict possible collisions with scene geometry.  

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section*{APPENDIX}


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section*{ACKNOWLEDGMENT}

% Put sponsor acknowledgments in the unnumbered footnote on the first page.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{IEEEtran}
\bibliography{danvil}

\end{document}
