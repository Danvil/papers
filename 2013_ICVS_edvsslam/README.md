
Remarks
----

Put *final* publication images in subfolder 'images' and add them to git repository. This is the images folder used by pdflatex.

Put experimental, temporary, source or raw images and files into the Wuala shared folder 'Science/papers/2013_icvs_edvsslam'. This is the working directory.

Example: You create an svg with inkscape. Put the svg file in the working directory and the pdf for usage with latex into the images directory.


Build instructions
----

Use makefile for convenience.

Normal build:

	make

Full build for updating bibliography:

	make full


Links
----

* [ICVS conference webpage](http://workshops.acin.tuwien.ac.at/ICVS/index.html)
* [LNCS author instructions](http://www.springer.com/computer/lncs?SGWID=0-164-6-793341-0)
