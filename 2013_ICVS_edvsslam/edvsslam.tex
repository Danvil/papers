%!TEX root = edvsslam.tex

% This is LLNCS.DEM the demonstration file of
% the LaTeX macro package from Springer-Verlag
% for Lecture Notes in Computer Science,
% version 2.4 for LaTeX2e as of 16. April 2010
%
\documentclass{llncs}
%
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\graphicspath{{./images/}{./}}
%\usepackage{caption,subcaption}
\usepackage{hyperref}
\usepackage[english]{babel}
\usepackage{balance}
\usepackage{booktabs}
\usepackage{array}
%
% decorate an expression with time
\newcommand{\wt}[1]{#1^{(k)}}
\newcommand{\wtp}[1]{#1^{(k-1)}}
\newcommand{\wtz}[1]{#1^{(0)}}
%
\begin{document}
%
\title{Simultaneous Localization and Mapping\\for event-based Vision Systems}
%
%\author{Double-blind Paper Submission 72}
\author{
	David Weikersdorfer
	\and Raoul Hoffmann
	\and J\"org Conradt
}
%
\institute{
	Neuroscientific System Theory\\
	Technische Universit\"at M\"unchen\\
	~\\
	\email{weikersd@in.tum.de ~ raoul.hoffmann@mytum.de ~ conrad@tum.de}
}
%
\maketitle
%
\begin{abstract}
We propose a novel method for vision based simultaneous localization and mapping (vSLAM) using a biologically inspired vision sensor that mimics the human retina.
The sensor consists of a 128x128 array of asynchronously operating pixels, which independently emit events upon a temporal illumination change.
Such a representation generates small amounts of data with high temporal precision; however, most classic computer vision algorithms need to be reworked as they require full RGB(-D) images at fixed frame rates.
Our presented vSLAM algorithm operates on individual pixel events and generates high-quality 2D environmental maps with precise robot localizations.
We evaluate our method with a state-of-the-art marker-based external tracking system and demonstrate real-time performance on standard computing hardware.
\end{abstract}
%

%
\section{Introduction}
%

Estimating your position within an environment is a crucial task for any autonomous artificial mobile system.
In mobile robotics, intense research and development efforts have been devoted to systems that localize themselves on a map while creating and maintaining such an a-priori unknown map (simultaneously localization and mapping, SLAM, \cite{neira2008slam}).
Typical engineered systems rely on a combination of sensors, such as ego-motion estimation (e.g. odometry or inertial measurements), distance sensors (e.g. laser range finders or RGB-D cameras), or on vision based feature tracking to create precise environmental representations for self-localization.
An example of a visual SLAM method is KinectFusion \cite{newcombe2011kinectfusion}, a dense surface mapping and tracking algorithm which requires highly optimized code and GPU hardware to run in real-time for a quite small spatial volume.
This is typical for many state-of-the-art vision SLAM systems which perform extremely well with the drawback of high computational requirements: typical algorithms by far exceed the computing power provided by small autonomous robots and often perform offline, time-delayed data interpretation.

In this paper we present a novel approach to visual SLAM in unknown environments for a biologically inspired event-based vision sensor that operates in real time.
The integrated Dynamic Vision Sensor (DVS) \cite{lichtsteiner2008} is a neurobiologically inspired pixel-wise asynchronous vision sensor that generates elementary events (''pieces of information'') only for every temporal illumination change perceived by any pixel (see fig. \ref{fig:edvs_example}).
Such a visual representation generates small amounts of data with high temporal precision, typically faster than 30 micro-seconds.
Each individual event contains very little reliable information, as the underlying cause for this event might be any one of a large collection of possible state changes, or even pure noise.
Frame based visual SLAM methods which operate on sequences of traditional camera images have nearly full localization information in each new image.
With event-based sensors we only marginally update the systems belief about the true underlying state with every new event.
This allows a more efficient system design, but most classic computer vision algorithms need to be redesigned as they require full images at fixed frame rates.

In \cite{weikersdorfer2012edvs} an event-based particle filter algorithm for visual, event-based self-localization using the eDVS sensor has been proposed.
In that work a fixed and pre-build map stitched from photos has been used for navigation.
This is a severe restriction for real world applications as the map should be generated directly by the acting robotic system without need for manual intervention or additional sensors.
\begin{figure}[t!]
	\centering
	\includegraphics[width=0.46\textwidth]{edvs128_detail.jpg}~
	\includegraphics[width=0.45\textwidth]{edvs_example.jpg}
	\caption{Left: The event-based embedded Dynamic Vision Sensor (eDVS). Right: An example data reading. Each image shows the 128x128 sensor with pixel events integrated over 0.1 seconds. Light pixels indicate change of illumination from dark to light - dark pixels a change from light to dark.}
	\label{fig:edvs_example}
\end{figure}
In this paper we extend this event-based tracking algorithm to a complete event-based simultaneous localization and mapping method which generates a map automatically during self-localization (see fig. \ref{fig:slam}).
%Our method works directly on individual events without need for event memory or event integration.

The rest of the paper is outlined as follows:
In \S\ref{sec:slam} we describe the concept behind our event-based simultaneous localization and mapping method.
Details about our main contribution - the mapping - will be presented in \S\ref{sec:mapping} in general and specialized to the case of 2D SLAM.
We evaluate our method on a large dataset in \S\ref{sec:evaluation} and show that we can achieve excellent tracking and mapping results.

%
\section{Event-based SLAM}
\label{sec:slam}
%

In \cite{weikersdorfer2012edvs} a novel event-based tracking algorithm which adapts the particle filter Condensation algorithm \cite{isard1998condensation} to the characteristics of an event-based sensor like the eDVS is proposed.
An event-based sensor provides a continuous stream of events $\wt{e}, e^{(k+1)}, \ldots \in \mathcal{R}$.
$\mathcal{R}$ denotes the range of possible pixel positions on the sensor, i.e. $\mathcal{R}=[0,127]\,^2$, and upper subscription is used to indicate event index.
Due to the fact that the number of events created by the event-based sensor depends on movement and rotation speed, the event index does not correspond to system time.

For each event $\wt{e} \in \mathcal{R}$, the event-based tracking algorithm in \cite{weikersdorfer2012edvs} uses a particle set $\wt{P} \subset \Omega \times \mathbb{R}^{+}$ for a multivariate probabilistic estimate of the current system state.
Each particle $p=(x,s) \in \wt{P}$ consists of the corresponding state estimate $x \in \Omega$ (position and orientation of the sensor) and a score value $s \in [0,1]$ representing the likelihood of the state estimate.
State likelihoods sum up to 1 for a given particle set.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.47\textwidth]{scenario_SLAM.pdf}
	\includegraphics[width=0.4\textwidth]{SLAM.pdf}
	\caption{Schematic overview of our tracking scenario for event-based tracking (left) and corresponding program flow (right).}
	\label{fig:slam}
\end{figure}

Towards the goal of simultaneous localization and mapping, we introduce a dynamic map over the map domain $\Gamma$ ($\Gamma=\mathbb{R}^2$ for a two-dimensional map), which our event-based SLAM method will continuously update during localization.
This is a major extension of the event-based tracking method in \cite{weikersdorfer2012edvs} as it is no longer required to manually provide an a priori map.
For each location $u \in \Gamma$ on the map we represent the likelihood that the event-based sensor will generate an event when one of it pixels observes this location while the sensor is moving.
Formally we model this likelihood as
\begin{equation}
	\mathcal{M}(u) = \frac{\# \text{ of occurred events for } u}{\# \text{ of possible obervations for } u} =: \frac{\mathcal{O}(u)}{\mathcal{Z}(u)}
	\label{eq:M}
\end{equation}
It is important to note the difference between the event-based sensor and a classic image sensor:
While a classic image sensor reports observations at fixed time intervals which can also be easily repeated by measuring multiple times, the event-based sensor generates events only if one of its pixels detects a change in illumination.
This happens either when objects move in the scene or, as in our case, when the sensor moves within a static environment and one of its pixels traverses an edge in the perceived brightness of the scenery.
Therefore map likelihood $\mathcal{M}$ measures the number of events generated at a map location with respect to the number of sensor pixel crossings.
As locations on the map can be observed several times with varying frequency, the occurrence map $\mathcal{O}$ needs to be normalized using a normalization map $\mathcal{Z}$ in order to get stable and comparable map likelihoods.

Our event-based localization and mapping method is outlined as follows:
For every event, we alternately update the state estimates using the latest map and update the map using the new state estimate (see fig. \ref{fig:slam}).
First, for event-based localization, we update the particle set $\wt{P}$ using the current event $\wt{e}$ and the likelihood map $\wtp{\mathcal{M}}$ from the previous step provided by event-based mapping.
State estimates $x_i$ are propagated using a diffusion model specific to the event-based sensor and state probabilities $s_i$ are computed using the likelihood map:
\begin{equation}
	\wt{s_i} = \wtp{s_i} + \alpha \, \wtp{\mathcal{M}}(\mu^{-1}(\wt{e}|\wt{x_i}))
\end{equation}
(see \cite{weikersdorfer2012edvs} for details).
The projection function $\mu : \Gamma \times \Omega \rightarrow \mathcal{R}$ and its inverse are discussed in more detail in \S\ref{sec:mapping}.
Second, the map $\wt{\mathcal{M}}$ is updated using the current event $\wt{e}$ and the current state estimate $\wt{P}$.
The second step, event-based visual mapping, is the major contributions of this paper and explained in more detail in section \S\ref{sec:mapping}.

%
\section{Event-based Visual Mapping}
\label{sec:mapping}
%

For the computation of the likelihood map $\mathcal{M}$, we iteratively compute three maps over the map domain $\Gamma$ during the mapping phase of our event-based SLAM method (see fig. \ref{fig:map}):
First, the occurrence map $\mathcal{O}: \Gamma \rightarrow \mathbb{R}^{+}$ where observation probabilities for events are summed up as events occur.
Second, the normalization map $\mathcal{Z}: \Gamma \rightarrow \mathbb{R}^{+}$ which records the possibility of observations depending on the magnitude of movement of the sensor.
Finally occurrence and normalization map are combined to the likelihood map $\mathcal{M}: \Gamma \rightarrow \mathbb{R}^{+}$ with eq. \ref{eq:M}.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.99\textwidth]{maps.png}
	\caption{Example for occurrence map $\mathcal{O}$ (left), normalization map $\mathcal{Z}$ (middle) and final likelihood map $\mathcal{M}$ (right).}
	\label{fig:map}
\end{figure}

\subsection{Computation of the occurrence map $\mathcal{O}$}
For the computation of the occurrence map, we project events from the sensor position back onto the map domain and integrate them using a Gaussian sensor model.
The current event $\wt{e}$ and the current state estimate $\wt{P} = \{\wt{p_1}, \ldots, \wt{p_n}\}$, $\wt{p_i} = (\wt{x_i}, \wt{s_i})$, are used to compute corresponding observed map locations $\mu^{-1}(\wt{e} | \wt{x_i})$.
Here we use the inverse of a projection function $\mu : \Gamma \times \Omega \rightarrow \mathcal{R}$ which projects a map location onto the sensor given a fixed state.
We assume that $\mu^{-1}$ has a unique solution - for a discussion see \S\ref{sec:mapping2d}.
Thus the occurrence map is computed iteratively as
\begin{equation}
	\wt{\mathcal{O}}(u) = \wtp{\mathcal{O}}(u) + \sum_{i=1}^{n}{\wt{s_i} \, \mathcal{N}\left(u \,\middle|\, \mu^{-1}(\wt{e} | \wt{x_i}), \sigma\right)}\,, \; \wtz{\mathcal{O}} = 0 \,.
	\label{eq:O}
\end{equation}
The Gaussian normal distribution $\mathcal{N}$ is used to represent measurement and discretization uncertainties.
The standard deviation $\sigma$ depends on camera parameters and the distance of the sensor to event location.
As generally done in SLAM algorithms, it can be beneficial to not use all particles for updating the occurrence map, but only a fraction with highest probabilities.

\subsection{Computation of the normalization map $\mathcal{Z}$}
The number of possible observations for the normalization map $\mathcal{Z}$ can be computed by considering the special properties of the event-based sensor:
Assuming a strong edge in the perceived light intensity, the sensor will generate one event for every pixel which passes over the edge.
Thus the fractional number of possible generated events for a map location $u \in \Gamma$ is proportional to the length of its path on the sensor in pixel coordinates.
Given a state estimate $x \in \Omega$, we compute the corresponding fractional pixel position on the sensor using the projection function $\mu$.
Note that this position does not necessarily lie inside the sensor boundaries as not all areas of the map are visible by the sensor at all times.
If a map point is not visible by the sensor under the current or previous state estimate the normalization map is not updated at this map location.
Otherwise the normalization map is computed as
\begin{equation}
	\wt{\mathcal{Z}}(u) = \wtp{\mathcal{Z}}(u) + \| \mu(u | \wt{x_{*}}) - \mu(u | \wtp{x_{*}}) \|\,, \; \wtz{\mathcal{Z}} = 0 \,.
	\label{eq:Z}
\end{equation}
$\wt{x_{*}}$ denotes the expected state which is computed as the weighted mean of the whole particle set $\wt{P}$.
Due to noise in the expected state and the high rate at which events are generated by the sensor, it is sensible to update the normalization map only periodically and not for every event.

% Finally the map probability can be computed as
% \begin{equation}
% 	\mathcal{M}^{(t)}(u) =
% 		\left\{\begin{array}{l l}
% 			\frac{\mathcal{O}^{(t)}(u)}{\mathcal{Z}^{(t)}(u)} ~~ & \text{if } \mathcal{Z}^{(t)}(u) > 0\\
% 			0 & \text{otherwise}
% 		\end{array} \right.
% \end{equation}

%
\subsection{Implementation example: Event-based SLAM for 2D scenarios}
\label{sec:mapping2d}
%
In this section we briefly explain how the general formulation of our event-based SLAM algorithm can be specialized for a 2D localization and mapping scenario as presented in the introduction (see fig. \ref{fig:slam}).
In this case, the state domain is the position of the robot on the floor and its rotation, i.e. $\Omega = \mathbb{R}^2 \times SO(2)$.
The map is constructed for a flat ceiling parallel to the floor on which the robot is moving, i. e. $\Gamma = \mathbb{R}^2$.

For this setting the projection function $\mu$ is realized using the pinhole camera model:
\begin{equation}
	\mu : \mathbb{R}^2 \times \mathbb{R}^2 \times SO(2) \rightarrow \mathcal{R}, \,
	(u, x, \theta) \mapsto \text{proj}\left(R_z(\theta)^{-1}\left(
		(u, D)^T - (x, 0)^T
		% \left(\begin{array}{c}u_x\\u_y\\D\end{array}\right) -
		% \left(\begin{array}{c}x_x\\x_y\\0\end{array}\right)
	\right)\right)
\end{equation}
with
\begin{equation}
	\text{proj} :
	\mathbb{R}^3 \rightarrow \mathcal{R}, \,
	v \mapsto \left( f\, \frac{v_x}{v_z} - c_x, f\, \frac{v_y}{v_z} - c_y \right)
\end{equation}
where $f$ and $c$ are the usual camera model parameters and $R_z(\theta)$ is rotation about the z-axis by an angle $\theta$.
$D$ is the constant height of the ceiling over the floor.

The standard deviation $\sigma$ in eq. \ref{eq:O} is chosen equal to half the size of a sensor pixel projected onto the ceiling.
This represents the fact that the size of the Gaussian on the occurrence map for an individual pixel event should correspond to the size of a sensor pixel projected onto the map space.

The inverse of the projection function $\mu^{-1}$, which indicates where the occurrence map is updated in eq. \ref{eq:Z}, has a unique solution when the distance $D$ is constant.
For the more general case where the distance of the source of an event to the sensor is not known this is no longer the case.
There are several possible strategies to solve this problem which are not further investigated in this paper:
The depth information could be provided by additional sensors like Primesens depth-sensing sensors \cite{shotton2011bodypartrecognition}, or the depth information could be estimated by using multiple event-based sensors \cite{schraml2010dynamic,rogister2012asynchronous}.
Another approach would be to use the full trace of the back projection $\mu^{-1}$ throughout a volumetric map using a cone-like probability distribution instead of a punctual Gaussian distribution in eq. \ref{eq:O}.
%This technique would count on condensation of likelihood through change of the view angle when the sensor is moving.

%
\section{Evaluation}
\label{sec:evaluation}
%
In order to test our method we equipped a mobile robot with an upwards facing event-based dynamic vision sensor (eDVS) as depicted in fig. \ref{fig:slam}.
The robot was remotely driven on varying paths through an indoor environment.
For ground truth comparison, the robot was tracked externally by the marker-based motion capture system OptiTrack V100:R2.
The event streams from the sensor were recorded and afterwards processed offline by our SLAM algorithm, resulting in estimated trajectories and constructed maps of the ceiling.
Our tracking system did not use any additional information such as user driving commands or wheel rotation measurements. 
Our implementation uses pixel gridmaps for occurrence map $\mathcal{O}$, normalization map $\mathcal{Z}$ and likelihood map $\mathcal{M}$ with a pixel size of 1 cm.

To evaluate the quality of trajectories resulting from our method, we compare them against the ground truth paths from the external tracking system by calculating the Root-mean-square error (RMSE) of the position error and the error in orientation angle.
As the world coordinate system of the external tracking system differs from the coordinate system chosen by our SLAM method we can only compare relative coordinates and thus have to first align our path to the ground truth path using a simple optimization over global position, global orientation, time offset and sensor rotation offset.
%Furthermore, we calculated the Euclidean distance between two corresponding points on the paths as the robot progresses on its trajectory, as well as the error in rotation.
%We can show that the error between the ground truth trajectory and our path dooes not accumulate over time and stays reasonable low.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.3\textwidth]{images/map_overlay_ceiling.jpg}
	\includegraphics[width=0.3\textwidth]{images/map_overlay_mapping_scalebar.jpg}
	\includegraphics[width=0.3\textwidth]{images/map_overlay_overlay.jpg}
	\caption{Left: Photo of the ceiling.
	Middle: Resulting map from our method. Darker spots indicating a higher likelihood of events. The green scale bar indicates the size of the field of view of the sensor on the ceiling (ca. 2 meters). 
	Right: Overlay of our map (magenta) and the edge map of the ceiling photo (blue).}
	\label{fig:map_overlay}
\end{figure}

\begin{table}[b!]
	\caption{Positional and rotational root-mean-square error (RMSE), failure rate and processing speed for a varying number of particles.}
	\centering
%		\begin{tabular}{p{1.5cm}|p{2.3cm}p{2.3cm}p{2.3cm}|p{2.3cm}}
		\begin{tabular}{p{1.5cm}|>{\raggedleft}p{2.0cm}>{\raggedleft}p{2.0cm}>{\raggedleft}p{2.0cm}>{\raggedleft\arraybackslash}p{2.0cm}}
%		\begin{tabular}{l|rrr|r}
			\toprule
			Particles & RMSE pos. & RMSE rot. & Failure rate & Events/s \\
			\midrule			
			 5 & 35.4 cm & 51.2$^\circ$ & 18/40 & 87800 \\ % 18/40 - 0.893 rad
			10 &  5.9 cm &  5.5$^\circ$ &  5/40 & 80700 \\ %  5/40 - 0.096 rad
			25 &  6.0 cm &  5.5$^\circ$ &  4/40 & 65600 \\ %  4/40 - 0.096 rad
			75 &  6.0 cm &  5.4$^\circ$ &  3/40 & 38800 \\ %  3/40 - 0.094 rad
			\bottomrule
		\end{tabular}
	\label{tab:particle_nums}
\end{table}

We evaluated our method on a dataset with 40 different randomly selected paths.
Figure \ref{fig:evaluation_examples} shows three typical examples and depicts the paths and maps created by our system, a comparison against ground truth as tracked by the overhead system and the corresponding error in position and orientation over time.
An overview over the mean RMSE in position and rotation, the number of successfully tracked scenarios \footnote{Scenarios are marked as not successful if a manual inspection shows a severe deviation of path and map from the the ground truth.} and the processed events per second is evaluated on the whole dataset against a varying number of particles (table \ref{tab:particle_nums}).
Reported mean errors of 6.0 cm and 5.5 degrees are expected regarding the low sensor resolution of 128x128 pixels and the height of the ceiling of about three meters.
The reported number of processed events per second demonstrate the efficiency of our method as in normal operating mode the sensor typical generates only around 25000 events per second.  

Fig. \ref{fig:map_overlay} shows that maps created by our method are clearly resembling reality by comparing against a photo of the ceiling.
As our map captures illumination changes we show an overlay of our map and an edge map of the photo created with the Sobel edge detector.
As shown on with the scale bar in the figure, the mapped region is multiple times larger than the field of view of the sensor.
It is observable that the likelihood $\mathcal{M}$ is higher at points of high local contrast in the photo.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.3\textwidth]{images/different_ceilings_ceiling.jpg}
	\includegraphics[width=0.3\textwidth]{images/different_ceilings_map_scalebar.jpg}
	\includegraphics[width=0.3\textwidth]{images/different_ceilings_overlay.jpg}
	\caption{Left: Photo of an indoor ceiling in a common office.
	Middle: Map and path created by our method. The green bar indicates the size of the field of view of the sensor on the ceiling (ca. 3 meters). 
	Right: Overlay of our map (magenta) and the edge map of the ceiling photo (blue).}
	\label{fig:different_ceilings}
\end{figure}

The ceiling of the room where the external tracking system is installed has various natural and artificially added distinctive features and edges.
To show that our method is also suitable with ceilings with fewer features, e.g. only overhead lamps, we tested it in a common office room.
A photo of the ceiling, the estimated robot path and the map created by our method and a map comparison is shown in fig. \ref{fig:different_ceilings}.
This demonstrates that even though our method relies on variations of brightness in the scenery, i.e. the ceiling, and works best with high feature density, it also produces correct results for common office ceilings with a low feature density.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.3\textwidth]{images/evaluation_examples_24_map.jpg}
	\includegraphics[width=0.3\textwidth]{images/evaluation_examples_24_combined.pdf}
	\includegraphics[width=0.3\textwidth]{images/evaluation_examples_24_errors.pdf}

	\includegraphics[width=0.3\textwidth]{images/evaluation_examples_40_map.jpg}
	\includegraphics[width=0.3\textwidth]{images/evaluation_examples_40_combined.pdf}
	\includegraphics[width=0.3\textwidth]{images/evaluation_examples_40_errors.pdf}

	\includegraphics[width=0.3\textwidth]{images/evaluation_examples_21_map.jpg}
	\includegraphics[width=0.3\textwidth]{images/evaluation_examples_21_combined.pdf}
	\includegraphics[width=0.3\textwidth]{images/evaluation_examples_21_errors.pdf}
	\caption{Top to bottom: Three typical examples out of a total of 40 from the dataset.
	Left: Map and path as created by our method.
	Middle: Trajectories resulting from our method (red) and the external tracking system (blue). The trajectory starting point is marked with a X.
	Right: Positional and rotational error over number of resamples.}
	\label{fig:evaluation_examples}
\end{figure}

%
\section{Conclusion}
\label{sec:conclusion}
%
We presented a novel method for simultaneous localization and mapping for systems equipped with an event-based vision sensor (DVS).
This sensor asynchronously reports individual events for perceived changes of pixel illumination.
Due to the sparse nature of data reported by such a sensor, we achieved a significant reduction in required computing resources compared to current state-of-the-art visual SLAM algorithms \cite{neira2008slam}, allowing faster than real-time localization and map generation (table \ref{tab:particle_nums}) on a single core desktop computer.
We are currently investigating a time-optimized implementation that runs in real time on standalone microcontroller boards for possible commercial applications.

%This in effect works like a low-pass filter that implicitly generates smooth trajectories.

%The presented method does not incorporate any explicit motion model, neither in localization nor in map generation.
%In the test case scenario ($\S$\ref{sec:evaluation}) we have in principle access to the mobile robots executed motion and could use that as a bias for new particle distributions (we believe this would further reduce the number of required particles for reliable localization estimates).
%Instead, here we only assume a random diffusion motion model for particle propagation, which allows application of our system as pure stand-alone add-on module on arbitrary existing hardware.

For this paper we applied our method to a 2D visual SLAM scenario where a mobile robot moves on the ground and continuously localizes itself using features on the ceiling.
Evaluation on a large recorded dataset demonstrates that our method works reliable with high accuracy already for a small number of particles.

\section*{Acknowledgements}

The authors would like to thank Brennand Pierce and Gorden Cheng from the Institute for Cognitive Systems (TUM) for granting us access to the overhead tracking system.
We also thank Nicolai Waniek (NST, TUM) and Jan Funke, Florian Jug, Michael Pfeiffer and Matthew Cook from the Institute of Neuroinformatics (ETH and University Zurich) for valuable discussions.

%
%\pagebreak
%
\bibliographystyle{splncs}
\bibliography{danvil_edvs}
%

\end{document}
