%!TEX root = exploration.tex


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

%\documentclass[letterpaper, 10 pt, conference]{ieeeconf}
\documentclass[a4paper, 10pt, conference]{ieeeconf}

\IEEEoverridecommandlockouts
% This command is only needed if you want to use the \thanks command

\overrideIEEEmargins
% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document


\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\graphicspath{{./images/}{./}}
% \usepackage{hyperref} %auskommentiert für pdf-check
\usepackage[english]{babel}
\usepackage{balance}
\usepackage{booktabs}
\usepackage{array}


\newcommand{\wt}[1]{#1^{(k)}}
\newcommand{\wtp}[1]{#1^{(k-1)}}
\newcommand{\wta}[2]{#1^{(#2)}}

\newcommand{\logfnc}{L}
\newcommand{\logfncA}[2]{
	\logfnc_{#2}\left(
		\frac{{#1}_\text{mean}-{#1}(v)}
		{{{#1}_\text{max}}-{{#1}_\text{min}}}
	\right)}
\newcommand{\gconf}{\mathcal{C}}
\newcommand{\gexpl}{\mathcal{E}}
\newcommand{\gain}{G}


\title{\LARGE \bf
	Autonomous Indoor Exploration with an \\ Event-Based Visual SLAM System
}

\author{Raoul Hoffmann$^{1}$, David Weikersdorfer$^{1}$ and Jorg Conradt$^{1}$% <-this % stops a space
	%\thanks{*This work was not supported by any organization}% <-this % stops a space
	\thanks{$^{1}$Neuroscientific System Theory (NST), Department of Electrical Engineering and Information Technology, Technische Universit\"at M\"unchen, Karlstr. 45, 80333 M\"unchen, Germany
	{\tt\small raoul.hoffmann@mytum.de}, {\tt\small weikersd@in.tum.de}, {\tt\small conradt@tum.de} }%
}


\begin{document}

\maketitle
\thispagestyle{empty}
\pagestyle{empty}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}

In this paper we present an autonomous mobile robot setting that automatically explores and maps unknown indoor environments, exclusively with information from an embedded event-based dynamic vision sensor (eDVS) and a ring of bump switches on the robot.
The eDVS provides a sparse pre-processed visual signature of the currently visible patch of ceiling, which is used for real-time simultaneous localization and mapping (SLAM).
Signals from the robot's bump switches together with its current position estimate continuously improve the system's reasoning about traversable areas.
A heuristic path planning method motivated by the $A^{*}$ search algorithm generates routes for continuous autonomous exploration.
We demonstrate robust real-time operation and evaluate the performance of our system in various indoor environments.

\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}

Estimating your position within an a-priori unknown environment is a crucial task for any autonomous mobile robot.
Intense research and development efforts have been devoted to systems that autonomously localize themselves on a map while simultaneously creating and maintaining such an a-priori unknown map \cite{neira2008slam}. 
Typical engineered systems rely on a combination of different sensors to create precise environmental representations for self-localization; but for various robotic applications a purely visual solution would be preferred. 
Any such vision based system to date requires highly optimized code and possibly GPU hardware to run in real-time even for quite constrained environments.
Most such algorithms by far exceed available computing power on small autonomous robots, and therefore often execute offline or in remote computing settings.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.90\columnwidth]{edvs_exploration.pdf}
	\caption{Overview of our autonomous exploration system: The robot localizes itself while driving (red) using only events from the event-based vision sensor (green).
	During exploration it detects obstacles using a bump sensor (orange) and avoids them for further exploration (magenta) while maximizing it's knowledge over the environment and creating a map for continuous self-localization (yellow).}
	\label{fig:intro}
\end{figure}

\begin{figure*}[t!]
	\centering
	\includegraphics[width=0.90\textwidth]{maps.png}
	\caption{Example for occurrence map $\mathcal{O}$ (left), normalization map $\mathcal{Z}$ (middle) and final likelihood map $\mathcal{M}$ (right).}
	\label{fig:map}
\end{figure*}

In this paper we apply a novel algorithmic approach to perform autonomous exploration and visual localization and mapping in unknown indoor environments in real time, using only an event-based embedded dynamic vision sensor (eDVS) and simple contact switches on the robot.
The eDVS \cite{lichtsteiner2008} is a neurobiologically inspired pixel-wise asynchronous vision sensor that generates elementary events (``pieces of information'') for any perceived local temporal illumination change. 
Such a visual representation generates sparse data with high temporal precision, typically in the micro-second range.
Each individual event contains little reliable information, as the underlying cause for this event is highly ambiguous or even pure noise.
%In contrast to such events, traditional vision data (each new camera frame) contains nearly full localization information required for visual SLAM (vSLAM).
With event-based sensors we only marginally update the system's belief about the true underlying state with every new event.
This allows a more efficient system design, but most classic computer vision algorithms need to be redesigned as they require full images at fixed frame rates.

In \cite{weikersdorfer2013edvsslam} a purely event-based method for visual simultaneous self-localization and mapping (vSLAM) using only a single eDVS sensor has been proposed.
We extend this method to an autonomous system performing exploration of unknown indoor environments with a basic heuristic exploration strategy that (a) seamlessly integrates into the existing framework of \cite{weikersdorfer2013edvsslam}, (b) removes the need for a human to carefully operate the robot during map building and (c) demonstrates the robustness of the event-based vSLAM method.

The paper is outlined as follows: chapter II introduces the developed event-based visual SLAM algorithm, chapter III discusses the proposed autonomous exploration strategy and its integration in the existing system, chapter IV presents and discusses exploration results, whereas chapter V briefly summarizes the benefits of our system.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{EVENT-BASED SIMULTANEOUS LOCALIZATION AND MAPPING}

We proposed a novel simultaneous localization and mapping (SLAM) method for event-based vision sensors in \cite{weikersdorfer2013edvsslam} and \cite{weikersdorfer2012edvs}.
An event-based vision sensor like the embedded dynamic vision sensor (eDVS) \cite{conradt2009} provides a stream of pixel events.
Pixel events happen due to a change in brightness at an individual pixel on the sensor independently of all other pixels.
This stands in contrast to classic computer vision which mainly works on full images at a fixed framerate.
The main advantage of event-based vision in general is the low power profile and the sparse image representation through events only representing the change in an image which allow a much more efficient system design.
However due to the different image representation, many classical computer vision methods have to be adapted or completely revised for event-based vision.
For example in comparison to KinectFusion \cite{newcombe2011kinectfusion} which uses full colour and depth frames and has to process data on a dedicated graphics card, our system which uses only pixel events runs several times faster than realtime on a single desktop CPU making it feasible for embedded applications.

In the stream of pixel events, each event is annotated with a high-resolution time measured in seconds where the natural latency of the eDVS is around $5 \,\mu s$.
However the time of events is not important for the referred SLAM method and for our exploration strategy.
When assuming a static environment, pixel events happen only due to a movement or rotation of the image sensor.
In particular the number of generated events for a given path only depend on the \emph{length} of the path but not on the \emph{time} required to move from start to end, i.e. it does not depend on the velocity.
Notably, this implies that no motion model is required as the moved distance can be inferred directly from the number of events.
See \cite{weikersdorfer2012edvs} for details. 

% \begin{figure}[t!]
% 	\centering
% 	\includegraphics[width=0.95\columnwidth]{SLAM.pdf}
% 	\caption{Schematic overview of the program flow of event-based simultaneous localization and mapping.}
% 	\label{fig:slam}
% \end{figure}

In the following the event-based SLAM method from \cite{weikersdorfer2013edvsslam} is reviewed as it forms the basis of the proposed autonomous exploration strategy.
We formalize the stream of pixel events as $\wta{e}{1}, \wta{e}{2}, \ldots, \wt{e}, \ldots \in \mathcal{R}$, where $\mathcal{R}$ denotes sensor coordinates, i.e. $\mathcal{R}=[{0|127}]^2$.
The event index $k$ is a continuous indexing of events and is not related to event time which is not required as explained previously.
The configuration space of the robot is $\Omega = \mathbb{R}^2 \times SO(2)$ and consists of the 2D position and orientation of the robot while it moves on the floor.
The tracked path is a sequence of positions $\wt{x} \in \Omega$ where a location estimate is provided for each event $k$.
The map is generated over a map space $\Gamma$, in our case this is the two-dimensional ceiling, i.e. $\Gamma = \mathbb{R}^2$.

The map $\mathcal{M} : \Gamma \rightarrow \mathbb{R}^{+}$ is a probabilistic model which represents the likelihood that the event-based sensor will generate an event at a given map location when one of it pixels observes this location while the sensor is moving.
According to \cite{weikersdorfer2013edvsslam}, $\mathcal{M}$ is computed from an occurrence map $\mathcal{O}$ and a normalization map $\mathcal{Z}$ (see eq. \ref{eq:M}) as follows:
\begin{equation}
	\mathcal{M}(u) = \frac{\# \text{ of occurred events for } u}{\# \text{ of possible obervations for } u} =: \frac{\mathcal{O}(u)}{\mathcal{Z}(u)}
	\label{eq:M}
\end{equation}
I.e. the map likelihood $\mathcal{M}$ measures the number of events with respect to the number of crossings of a sensor pixel.
As locations on the map can be observed several times with varying frequency, the occurrence map $\mathcal{O}$ needs to be normalized using the normalization map $\mathcal{Z}$ in order to get consistent map likelihoods.

\begin{figure*}[t!]
	\centering
	% \includegraphics[width=0.31\textwidth]{work/planning/34_00080_confidence_plan.pdf}
	% \includegraphics[width=0.31\textwidth]{work/planning/34_00080_exploration_plan.pdf}
	% \includegraphics[width=0.31\textwidth]{work/planning/34_00080_cost_plan.pdf}
	\includegraphics[width=0.99\textwidth]{planning.png}
	\caption{In each figure the robot path (red), the current plan (magenta) and the actual room layout (black) are shown. {\bf Left:} Confidence values computed from $\mathcal{Z}$ provided by the SLAM algorithm (darker is more confident). {\bf Middle:} Exploration map (in grey: darker points indicate higher values) and blocked points (blue). {\bf Right:} Gain $\gain$ computed by equation \ref{eq:gain} (darker is higher).}
	\label{fig:planning}
\end{figure*}

The current event $\wt{e}$ and the current map $\wt{\mathcal{M}}$ are used during the event-based localization step to compute an update state estimate $\wt{x}$ using the model of a temporal Bayesian network and a particle filter to realize the discretisation \cite{isard1998condensation}.
Assuming a set of particles consisting of state estimates $\wt{x_i}$ and scores $\wt{s_i}$ representing the corresponding likelihood of the state, scores are updated using an exponential decay model after eq. \ref{eq:scores} (see \cite{weikersdorfer2012edvs} for details).
\begin{equation}
	\wt{s_i} = \wtp{s_i} + \alpha \, \wtp{\mathcal{M}}(\mu^{-1}(\wt{x_i}|\wt{e}))
	\label{eq:scores}
\end{equation}
The projection function $\mu : \Gamma \times \Omega \rightarrow \mathcal{R}$ projects a map location onto the sensor given a fixed state and is defined using the standard camera pinhole model.
The inverse $\mu^{-1}$, back-projecting a sensor location onto the map, is well defined for the 2D case.
States are updated by applying a motion model and by performing a weighted resampling based on particle scores.

In the second step, event-based mapping, the current event $\wt{e}$ and the updated set of particles is used to update the map.
The occurrence map is computed using the iterative formula
\begin{equation}
	\wt{\mathcal{O}}(u) = \wtp{\mathcal{O}}(u) + \sum_{i=1}^{n}{\wt{s_i} \, \mathcal{N}\left(u \,\middle|\, \mu^{-1}(\wt{e} | \wt{x_i}), \sigma\right)}
	\label{eq:O}
\end{equation}
with $\wta{\mathcal{O}}{0} = 0$.
Measurement and discretisation uncertainties of the camera model are compensated using a Gaussian normal distribution $\mathcal{N}$ where the standard deviation $\sigma$ depends on camera parameters and the distance of the sensor to the event location.
The normalization map is computed as
\begin{equation}
	\wt{\mathcal{Z}}(u) = \wtp{\mathcal{Z}}(u) + \| \mu(u | \wt{x_{*}}) - \mu(u | \wtp{x_{*}}) \| \,.
	\label{eq:Z}
\end{equation}
with $\wta{\mathcal{Z}}{0} = 0$.
Here the current expected state estimate $\wt{x_{*}}$ is computed as the weighted update of the particle set.
Fig. \ref{fig:map} visualizes occurrence, normalization and the final map.
For further details about the mapping step see \cite{weikersdorfer2013edvsslam}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{EXPLORATION}

For the presented event-based SLAM method, the normalization map $\mathcal{Z}$ represents how often a particular spot on the map was observed and thus indicates the confidence for robot self-localization.
Here, a uniform coverage of the whole space is more desirable than isolated extrema as it guarantees that the robot can orient itself in the whole space of interest and not only in particular areas.
Thus our exploration strategy directs the robot on paths that first increase the localization confidence in regions where it is currently low compared to other parts of the map.

Furthermore, we pursue to detect walls and other obstacles in the environment, as knowledge about the actual reachability of locations is substantial for planning collision-free paths.
Currently, information about obstacles can not be derived from the visual input because the event-based vision sensor is pointed upwards as it uses features on the ceiling for self-localization, but objects which block the robot are located on the floor.
However, in our case walls and obstacles can be detected by evaluating the robot's physical collision sensors which reports the location where the robot has collided with an obstacle.
For this reason we use a second map to represent the exploration status of the floor.
Here we store how often the robot has already explored a specific point on the ground, and where obstacle are located based on the bump information.
In the same way as for the confidence map, our exploration strategy tries to uniformly cover the whole space in the exploration map in order to build a good map about the environment.

The combination of these two goals leads to an autonomous exploration behaviour of the robot which constantly tries to increase its knowledge over the environment.
Therefore, there is no stopping condition involved, as the state is never regarded as fully explored.
By using self-localizations provided by the event-based vision sensor and combining this with information from the collision sensor we get a complete map of the environment which allows the robot to localize itself and to plan optimal collision-free paths.

\begin{figure*}[t!]
	\centering
	% \includegraphics[width=0.31\textwidth]{work/final_exploration/35_00400_static.pdf}
	% \includegraphics[width=0.31\textwidth]{work/final_exploration/34_00460_static.pdf}
	% \includegraphics[width=0.31\textwidth]{work/final_exploration/31_00250_static.pdf}
	\includegraphics[width=0.99\textwidth]{final_exploration.png}
	\caption{Three results from explorations in different room configurations. The figures show the robot trajectory (red), collision positions (orange circles), unreachable points (blue) and the actual room borders (black).}
	\label{fig:room_exploration}
\end{figure*}

For the implementation both the confidence and the exploration map are discretised into a hexagonal grid.
The hexagonal grid is used for convenience as all neighbours to a grid point are at the same distance and thus the generated path is smoother and easier to execute.
The confidence value $\gconf(v)$ is computed from the normalization map $\mathcal{Z}$ by integrating over a small region in $\mathcal{Z}$ for every grid point $v$.
$\gconf$ is updated continuously over time whenever the normalization map is updated by the SLAM algorithm.
The exploration value $\gexpl(v)$ is updated whenever the current position estimate $\wt{x_{*}}$ is updated and the robot moves to a new grid point $v$ - in this case the value for $\gexpl(v)$ is increased by one.
$\gexpl(v)$ thus represents the number of times a location $v$ was visited by the robot.
In case of a collision event, we mark the current grid position of the robot and several grid points around the collision point as inaccessible.

Both confidence and exploration map are combined to compute a potential gain value $\gain(v)$ which represents the gain in information when the robot drives to this specific location.
The gain in information for a node in the discretised grid is defined as
\begin{equation}
	\gain(v) := w_\gconf \, \logfncA{\gconf}{\alpha} + w_\gexpl \, \logfncA{\gexpl}{\alpha}
	\label{eq:gain}
\end{equation}
where $\logfnc$ is the logistic function defined as
\begin{equation}
	\logfnc_\alpha(x) := \frac{1}{1+e^{-\alpha\,x}}
\end{equation}
and $\gconf/\gexpl_\text{min}, \gconf/\gexpl_\text{mean}, \gconf/\gexpl_\text{max}$ denotes the minimum, mean and maximum over all values of the grid maps $\gconf$ and $\gexpl$ respectively.
For our experiments we chose $w_\gconf=w_\gexpl=1$ as weighting constants and $\alpha=10$.
Lower values of $\alpha$ stretch the graph of $\logfnc$ in x direction.
The sigmoid function assures that regions which are explored below average are preferred by our exploration strategy and normalizing with respect to the mean guarantees that the gain value automatically adapts to increasing values in the confidence and exploration maps over time.
Figure \ref{fig:planning} visualizes the computation of the gain map from confidence and exploration map for an example exploration scenario and demonstrates how unexplored areas are preferred by our exploration strategy.

In order to compute a path which maximizes the gain in information we use an algorithm motivated by the $A^{*}$ search algorithm \cite{hart1968formal}.
However as there is no goal point defined the cost heuristic is modified to consider the gain values $\gain$.
Our search algorithm for the optimal path starts with the grid point for the current robot position and iteratively ''opens'' and ''expands'' nodes in the language of the $A^{*}$ search algorithm.
When expanding a node, all accessible connected nodes are found, i.e. neighbouring grid points which are not marked as blocked by a previous bump signal from the robot collision sensor, and added to the list of currently open points.
The expanded node itself is ''closed'', i.e. removed from the list of open points.
Additionally for each node two values are stored and updated during expansion:
First, the path length measured in number of steps on the grid from the starting position of the robot is computed by increasing the path length for the current node by one.
Second, the total gain is computed by adding the gain of the current node and the gain value of the new grid point.
These two values are used to determine which node is picked next for expansion by choosing the node with the highest total gain per total path length.
We quit the expansion of points after maximal number of considered nodes and choose the path with the highest total gain per total path length which has a given minimal length.
The path itself can be computed by tracing back by which parent node a given node was expanded.
Replanning is executed whenever the robot reached its current goal or whenever the robot detects a collision by bumping into an obstacle.
In the latter case prior to replanning the robot is moved back from the collision point for a short distance to avoid continuous brushing along obstacles.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure*}[t!]
	\centering
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00020_map_path.pdf}
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00040_map_path.pdf}
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00153_map_path.pdf}
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00250_map_path.pdf}

	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00020_loc_exp.pdf}
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00040_loc_exp.pdf}
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00153_loc_exp.pdf}
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00250_loc_exp.pdf}
	
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00020_obst_exp.pdf}
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00040_obst_exp.pdf}
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00153_obst_exp.pdf}
	% \includegraphics[width=0.24\textwidth]{work/time_series/31_00250_obst_exp.pdf}
	\includegraphics[width=0.99\textwidth]{time_series.png}
	\caption{Example of an exploration sequence. The robot trajectory (red), the currently planned path (magenta dots) and the actual room layout (black) are shown in each figure. In addition shown in the 
	{\bf upper row:} SLAM result map $\mathcal{M}$ of the ceiling features
	{\bf centre row:} confidence map $\mathcal{C}$ sampled from the SLAM normalization map $\mathcal{Z}$
	{\bf bottom row:} exploration map $\mathcal{E}$, collision positions (orange circles) and unreachable points (blue)}
	\label{fig:time_series}
\end{figure*}

\section{EXPERIMENTAL RESULTS}

For evaluating our method we used a small mobile robot, sized similar to a robotic vacuum cleaner.
As sensors the robot only features an upwards facing event-based dynamic vision sensor (eDVS) and six collision sensors arranged on a bumper ring.
We implemented the event-based SLAM method and our exploration strategy to run online in real time on a desktop computer connected to the robot via Wireless LAN. 
Events from the eDVS were streamed to the desktop computer and processed by the SLAM method to compute the current map and position.
Given the map and robot trajectory, our exploration strategy decided on a reasonable path to follow and the associated steering commands were sent back to the robot over the network.
For the exploration map $\mathcal{E}$ and confidence map $\mathcal{C}$ we chose a spatial resolution of the hex grid of $10\,\text{cm}$.
With the robot moving at its full speed of about $0.1\,\text{m/s}$, the eDVS generates approximately 20.000 events per second.
We measured our method to process the stream of events at a rate of 50.000 per second, therefore running at $2.5 \times$ real-time pace and proving suitable for on-line use.
Our real world experiments took place in a room where we set up artificial obstacles to assess various wall configurations.
The region left to the robot to explore spanned a total area of $9.6\,\text{m}^2$, consisting of a rectangular shaped zone measuring $3.0\,\text{m} \times 3.2\,\text{m}$.
In the beginning of each run the robot was placed inside the area, left there to explore it, and stopped some time after all obstacles were sufficiently reconstructed.
In a series of 35 experiments in several different room setups, our method robustly produced proper reconstructions of the walls and obstacles.
Three typical examples of exploration runs with different room configurations are shown in fig. \ref{fig:room_exploration}, emphasizing that the points marked as unreachable resemble the actual room borders.
In addition, the robot's trajectory, the resulting map as well as the positions and normals of collisions are depicted.
It can be recognized that the method manages to find paths that avoid recent positions of the robot, resulting in a good coverage of open space.
Fig. \ref{fig:time_series} shows a time sequence illustrating an examplary exploration process of the robot. 
As expected, the confidence map $\mathcal{C}$ is spread out over an growing area and distributed more uniformly over time. 
Furthermore it can be observed how room borders are detected and reconstructed from the robot's collisions with obstacles.
At average, it took the robot 10 minutes to fully determine the obstacles in its environment.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{CONCLUSIONS}

We presented an exploration method as an application for event-based vSLAM using a biologically inspired low-profile sensor. 
The method was thoroughly assessed in real world experiments and proved to robustly reconstruct obstacles in the environment while improving the quality of the map by preferably navigating the robot towards points of lower knowledge. 
In the current implementation all computation happens on an off-board PC via a wireless connection, but due to the high efficiency of our method it could be easily transferred on-board to run on embedded computation hardware.
The presented successful autonomous exploration, localization and mapping highlights the efficiency and robustness of our event-based vSLAM algorithm \cite{weikersdorfer2013edvsslam}, as the consistent creation of obstacle maps requires precise localization within the initially unknown map.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section*{ACKNOWLEDGMENT}

The authors would like to thank everyone from the Neuroscientific Systems Theory Group for their continuous support and for always giving a helpful hand.
Moreover we would like to thank Jan Funke, Florian Jug, Michael Pfeiffer and Matthew Cook from the Institute of Neuroinformatics (ETH and University Zurich) for valuable discussions.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section*{SUPPLEMENTARY MATERIAL}

% The video in the supplementary material shows the continous self-localization (red path), the detected obstacles (orange and blue) and the currently planned path (magenta) as a video over time.
% The playback speed is 15 times realtime.


% \addtolength{\textheight}{-22.3cm}
 % This command serves to balance the column lengths on the last page of the document manually.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,bib_exploration}


\end{document}
