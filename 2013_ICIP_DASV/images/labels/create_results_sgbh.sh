srcdir="$1"
sgbhdir="$2"
dstdir="$3"

mkdir "$dstdir"

imgArr=(`ls $srcdir/*_segs_labels.png`)
imgcount=${#imgArr[*]}
echo $imgcount

imgColor=(`ls $srcdir/*_color.png`)
imgLabels=(`ls $srcdir/*_segs_labels.png`)
imgSbgh=(`ls $sgbhdir/*.ppm`)

crop="[520x390+60+45]"
geom="-geometry 260x195+0+0"

offset=0
start=16
delta=16
# offset=-194
# start=200
# delta=30

k=0
for ((i=$start;i<$imgcount;i=$i+$delta)); do
	echo "Montaging $i"
	montage ${imgColor[$i]}${crop} ${imgLabels[$i]}${crop} ${imgSbgh[$((i+offset))]}${crop} -tile 1x3 ${geom} $(printf "$dstdir/m_%01d.jpg" ${k})
	k=$((k+1))
done
