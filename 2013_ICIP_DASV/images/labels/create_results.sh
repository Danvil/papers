#imagedir="/tmp/strands"
srcdir="$1"
dstdir="$2"

imgArr=(`ls $srcdir/*labels.png`)
imgcount=${#imgArr[*]}
echo $imgcount

imgColor=(`ls $srcdir/*color.png`)
imgDepth=(`ls $srcdir/*depth.png`)
imgDasp=(`ls $srcdir/*dasp.png`)
imgLabels=(`ls $srcdir/*labels.png`)

crop="[520x390+60+45]"
geom="-geometry 260x195+0+0"

k=0
for ((i=10;i<$imgcount;i=$i+30)); do
	echo "Montaging $i"
	montage ${imgColor[$i]}${crop} ${imgDepth[$i]}${crop} ${imgDasp[$i]}${crop} ${imgLabels[$i]}${crop} -tile 1x4 ${geom} $(printf "$dstdir/m_%01d.jpg" ${k})
	k=$((k+1))
done
