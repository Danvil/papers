#dirv="/home/david/Desktop/dasv/1000-3.0/Person-Hand-4"
dirv="/media/smallfile/dasv/dasv-laptop/1000-3.0/Person-Hand-4"
dirs="/media/smallfile/dasv/streamgbh/Person-Hand-4"
dst="./explain"

crop="[123x200+180+90]"
geom="-geometry 100%x100%+0+0"

k=52

iv=$(printf "%05d" ${k})
is=$(printf "%05d" $((k+1)))

img1="${dirv}/${iv}_color.png"
img2="${dirv}/${iv}_depth.png"
img3="$dirv/${iv}_dasv_labels.png"
img4="$dirv/${iv}_segs_labels.png"

montage ${img1}${crop} ${img2}${crop} \
	-tile 2x1 ${geom} "$dst/explain1.jpg"

#convert ${img3}${crop} +repage -gravity south
convert ${img3}${crop} +repage -gravity south \
	-stroke '#000C' -strokewidth 2 -annotate 0 'n=992' \
	-stroke none -fill white -annotate 0 'n=992' "$dst/tmp3.jpg"
#convert ${img3}${crop} +repage -gravity south
convert ${img4}${crop} +repage -gravity south \
	-stroke '#000C' -strokewidth 2 -annotate 0 'n=133' \
	-stroke none -fill white -annotate 0 'n=133' "$dst/tmp4.jpg"

montage \
"$dirv/${iv}_dasp.png${crop}" \
"$dirv/${iv}_dasv_graph.png${crop}" \
-tile 2x1 ${geom} "$dst/explain2.jpg"

montage \
"$dst/tmp3.jpg" \
"$dst/tmp4.jpg" \
-tile 2x1 ${geom} "$dst/explain3.jpg"

# convert "$dirs/00/$is.ppm"${crop} +repage -gravity south \
# 	-stroke '#000C' -strokewidth 2 -annotate 0 'n=3126' \
# 	-stroke none -fill white -annotate 0 'n=3126' "$dst/tmps1.jpg"
# convert "$dirs/06/$is.ppm"${crop} +repage -gravity south \
# 	-stroke '#000C' -strokewidth 2 -annotate 0 'n=2292' \
# 	-stroke none -fill white -annotate 0 'n=2292' "$dst/tmps2.jpg"
convert "$dirs/11/$is.ppm"${crop} +repage -gravity south \
	-stroke '#000C' -strokewidth 2 -annotate 0 'n=971' \
	-stroke none -fill white -annotate 0 'n=971' "$dst/tmps3.jpg"
# convert "$dirs/13/$is.ppm"${crop} +repage -gravity south \
# 	-stroke '#000C' -strokewidth 2 -annotate 0 'n=576' \
# 	-stroke none -fill white -annotate 0 'n=576' "$dst/tmps4.jpg"
convert "$dirs/18/$is.ppm"${crop} +repage -gravity south \
	-stroke '#000C' -strokewidth 2 -annotate 0 'n=118' \
	-stroke none -fill white -annotate 0 'n=118' "$dst/tmps5.jpg"

#"$dst/tmps1.jpg" "$dst/tmps2.jpg" "$dst/tmps3.jpg" "$dst/tmps4.jpg" "$dst/tmps5.jpg" \

montage \
"$dst/tmps3.jpg" "$dst/tmps5.jpg" \
-tile 2x1 ${geom} "$dst/explain4.jpg"
