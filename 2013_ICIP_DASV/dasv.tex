%!TEX root = dasv.tex

% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{spconf}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{graphicx}
\graphicspath{{./images/}}
\usepackage{caption,subcaption}
\usepackage{hyperref}
\usepackage[english]{babel}
\usepackage{balance}
% --------------------------------------------------------------------------

% Title.
% ------
\title{DEPTH-ADAPTIVE SUPERVOXELS FOR RGB-D VIDEO SEGMENTATION}

\threeauthors
{David Weikersdorfer
\thanks{Thanks to Prof. J\"org Conradt from the Neuroscientific System Theory Group, Technische Universit\"at M\"unchen, for supporting this project.}}
{
	Neuroscientific System Theory\\
%	Technical University Munich\\
	Technische Universit\"at M\"unchen\\
%	Karlstr. 45, 80333 Munich, Germany}
%	\small\href{mailto:weikersd@in.tum.de}{\nolinkurl{weikersd@in.tum.de}}
}
{Alexander Schick}
{
	Fraunhofer IOSB\\
	Karlsruhe\\
%	Institute of Optronics, System\\Technologies and Image Exploitation\\
%	Address}
% 	\small\href{mailto:alexander.schick@iosb.fraunhofer.de}{\nolinkurl{alexander.schick@iosb.fraunhofer.de}}
}
{Daniel Cremers}
{
	Computer Vision \& Image Analysis\\
%	Technical University Munich\\
	Technische Universit\"at M\"unchen\\
%	Boltzmannstrasse 3, 85748 Garching, Germany}
%	\small\href{mailto:cremers@in.tum.de}{\nolinkurl{cremers@in.tum.de}}
}

% --------------------------------------------------------------------------

\begin{document}
%\ninept

\maketitle

% \setlength{\floatsep}{-2cm}
% \setlength{\dblfloatsep}{-2cm}
% \setlength{\intextsep}{-2cm}

\begin{abstract}
In this paper we present a method for automatic video segmentation of RGB-D video streams provided by combined colour and depth sensors like the Microsoft Kinect.
To this end, we combine position and normal information from the depth sensor with colour information to compute temporally stable, depth-adaptive superpixels
and combine them
%Our video analysis framework combines theses superpixels
into a graph of strand-like spatiotemporal, depth-adaptive supervoxels.
We use spectral graph clustering on the supervoxel graph to partition it into spatiotemporal segments.
% , effectively realizing tracking-by-segmentation.
Experimental evaluation on several challenging scenarios demonstrates that our two-layer RGB-D video segmentation technique produces excellent video segmentation results.
\end{abstract}

\begin{keywords}
Superpixels, Supervoxels, RGB-D, Video Segmentation, Video Analysis
\end{keywords}

% --------------------------------------------------------------------------

\section{Introduction}
\label{sec:intro}

Superpixel segmentation is an oversegmentation technique that received increasing attention in the last years. Superpixels align well with object boundaries and are generally of a compact shape. These properties allow using them as input in other algorithms instead of pixels. Their main advantage is the reduction of the input complexity from tens of thousands of pixels to only a couple of hundred superpixels. There exists a large variety of superpixel segmentation algorithms with different properties %and examples can be found in
\cite{levinshtein2009, achanta2010, perbet2011homogeneous, weikersdorfer2012dasp, shi2000ncut, schick2012compact}.

\begin{figure}[t!]
	\centering
	\includegraphics[width=0.90\columnwidth]{intro/vol.jpg}
	\caption{Our method computes coherent, spatiotemporal depth-adaptive supervoxels (lower left) and consistent temporal segments (lower right) for a RGB-D video stream (upper).}
	\label{fig:overview}
\end{figure}

% --------------------------------------------------------------------------

\newcommand{\figcrop}[1]{\includegraphics[trim=5cm 8cm 12cm 2cm,clip=true,width=0.12\textwidth]{#1}}

\begin{figure*}[t!]
	\centering
	\figcrop{temporal/img_color.jpg}
	\figcrop{temporal/img_depth.jpg}
	\figcrop{temporal/img_density.jpg}
	\figcrop{temporal/img_dasp.jpg}
	\figcrop{temporal/img_cluster_density_prev.jpg}
	\figcrop{temporal/img_density_curr.jpg}
	\figcrop{temporal/img_density_delta.jpg}
	\figcrop{temporal/cluster_change}
	\caption{{\bf From left to right}: Colour and depth input, superpixel density, depth-adaptive superpixels, cluster density, target density for new frame, difference between previous and new density (red resp. blue indicates that clusters have to be removed resp. added) and result of delta density sampling (red: removed superpixel, blue: added superpixel, black: moved superpixel).}
	\label{fig:stable}
\end{figure*}

% --------------------------------------------------------------------------

Supervoxels extend the planar superpixels into the third dimension by not only clustering the pixels in each image, but by segmenting a stack of images.
The image stack can either be composed by frames of volumetric scans (spatial image stack, e.g. medical scans) or by stacking video frames (temporal).
Examples include Veksler et al.~\cite{veksler2010} and Lucchi et al.~\cite{lucchi2010supervoxel}.
A recent survey and evaluation of supervoxel segmentations was given by Xu and Corso~\cite{xu2012evaluation}.
One advantage of using supervoxels is similar to superpixels: reduction of the number of primitives and grouping of similar pixels to one compact representation primitive.
The main application area for temporal supervoxels is video segmentation~\cite{xu2012streaming}.

Recently, a method was proposed for computing homogeneous, depth-adaptive superpixels (DASP) \cite{weikersdorfer2012dasp} for RGB-D images which are uniformly distributed on the surface of the 3D scene geometry. % and manifest on the pixel grid as distorted depth-adaptive superpixels.
%This means that each superpixel has approximately the same size in world coordinates independent of where it is positioned in the image.
As it considers depth in addition to colour, significantly better superpixels properties and image segmentations can be achieved. %Further, DASV are guaranteed to have approximately the same size in real-world space independent on their position in the image.

In this paper, we propose depth-adaptive supervoxels (DASV) which are the extension of DASP to the temporal domain.
DASV are formed as coherent strands of temporally stable depth-adaptive superpixels and build an oversegmentation of the temporal image stack.
We use spectral graph segmentation to segment the DASV into coherent segments to provide classic video segmentations.
Our method is thus a hierarchical video segmentation technique with only two layers: supervoxels and segments (see fig. \ref{fig:overview}).
%Due to the underlying properties of DASP in combination with our extension to image stacks based on spectral graph techniques, DASV are formed as strands of spatial coherent superpixels.
% We will demonstrate DASV in a tracking-by-segmentation framework by tracking various objects without prior knowledge simply by segmenting the DASV graph.
%We will demonstrate that DASV have a high stability even for moving objects without prior knowledge simply by segmenting the DASV graph.
It is unsupervised, model-free, runs in near realtime and shows very good results compared to the state of the art.

This paper is outlined as follows:
%In this section, we gave a short introduction to superpixel and supervoxel oversegmentation.
In $\S$\ref{sec:stable}, we introduce %an adaption to the cluster sampling strategy to produce
temporally stable, depth-adaptive superpixels. They are used in $\S$\ref{sec:segments} to construct a spatiotemporal supervoxel graph which is segmented using a spectral segmentation technique.
The paper will be concluded with an evaluation of our method in $\S$\ref{sec:evaluation}.
% and a short conclusion in $\S$\ref{sec:conclusion}.

% Recently we proposed a method for computing homogenous, depth-adaptive superpixels for RGB-D images \cite{weikersdorfer2012dasp}.
% This method provides superpixels which are uniformly distributed over the 3D surface computable from depth information and manifest on the pixel grid as distorted depth-adaptive superpixels.
% Considering depth in addition to colour guarantees significantly better superpixel properties, which is especially visible when using depth-adatipve superpixels together with spectral graph segmentation techniques to compute classic image segmentations.

% --------------------------------------------------------------------------

\section{Stable depth-adaptive superpixels}
\label{sec:stable}

Depth-adaptive superpixels are computed frame by frame in three steps (see fig. \ref{fig:stable}).
First, the depth-adaptive superpixel cluster density is computed from the depth input image.
Second, a Poisson disc sampling method (e.g. \cite{fattal2011}) is used to sample initial cluster centres.
Third, sampled cluster centres are used in an density-adaptive local iterative clustering algorithm to assign pixels to cluster centres.
For details see \cite{weikersdorfer2012dasp}.

It turns out that the extension of superpixels to the temporal domain is by no means straight-forward.
A naive frame-by-frame processing, for example, will lead to a severe jittering of superpixels over time because samples and superpixels are determined independently in each frame.
For most applications this jittering is quite undesirable.
The key idea of our approach is to induce a stable superpixel distribution by propagating density information over time.
%Due to the probabilistic process of density sampling superpixel positions are not stable from frame to frame.

%In this section we propose a method to sample temporally stable, approximatively poisson distributed superpixel seed points.
Our method has two main advantages for RGB-D video streams: On the one hand it is straightforward to establish temporal superpixel connections between consecutive frames and on the other hand the number of iterations of the nearest-neighbour pixel assignment step can be reduced for higher processing performance.

The key idea for our temporally stable sampling method is the comparison between the superpixel density $\rho_C$ provided from superpixel centres $C^{(t-1)}$ from previous the frame $t-1$ and the target superpixel density $\rho_D$ computed from the depth image $D^{(t)}$ of the current frame $t$ (see fig. \ref{fig:stable}).
$\rho_C$ is approximated using a classical kernel density estimator of the form
\begin{equation}
	\rho_C(x\,|\,C^{(t-1)}) = \sum_{i=1}^{n}{k_{\sigma_i}\left(\|x - x_i\|\right)}
	\label{eq:rhoC}
\end{equation}
with $C^{(t-1)} = \{ (x_i, \sigma_i) \}$, $x_i$ cluster position and $\sigma_i$ cluster scale.
In our case we use a two-dimensional gaussian kernel
\begin{equation}
	k_\sigma(d) = \frac{1}{\sigma^2} \, e^{-\pi\frac{d^2}{\sigma^2}} \,.
\end{equation}	
The depth-adaptive superpixels target density $\rho_D$ is
\begin{equation}
%	\rho_D(x\,|\,D^{(t)}) \propto \frac{1}{\pi} \sqrt{\|\nabla D^{(t)}(x)\|^2+1} \left(\frac{D^{(t)}(x)}{f}\right)^2
	\rho_D(x\,|\,D^{(t)}) \propto \left(D^{(t)}(x)\right)^2 \sqrt{\|\nabla D^{(t)}(x)\|^2+1}
	\label{eq:rhoD}
\end{equation}
which corresponds to the area of an infinitesimal surface element in 3D space.
$\rho_D$ is normalized such that the integral gives the desired number of superpixels.
The difference between theses two density functions
\begin{equation}
	\Delta \rho^{(t)}(x) = \rho_D(x\,|\,D^{(t)}) - \rho_C(x\,|\,C^{(t-1)}) \,.
	\label{eq:rho}
\end{equation}
 - a pseudo density function which can be both positive and negative - is used in a hierarchical sampling process like in \cite{fattal2011}.
The sampling process starts with all cluster centres $C^{(t-1)}$ from the previous frame.
During the sampling process there may occur two cases.
Either sample points are to be added, this is carried out normally, or sample points are to be removed.
In this case the point nearest to the location where a point shall be removed is found in the set of all points currently sampled and removed.
Fig. \ref{fig:stable} exemplarily shows the result of such a sampling process.

% --------------------------------------------------------------------------

\begin{figure*}[t!]
	\centering
	\includegraphics[width=0.24\textwidth]{strands.pdf}
	\includegraphics[width=0.24\textwidth]{eval_ced.pdf}
	\includegraphics[width=0.24\textwidth]{eval_cec.pdf}
	\includegraphics[width=0.24\textwidth]{eval_cmp.pdf}
	\caption{{\bf Left}: Creation of strand-like supervoxels from frame superpixels. Red/green are superpixels removed/added during sampling. Orange connections are to be tested. {\bf Middle}: Colour and depth explained variation metric for our method (red: supervoxels, blue: supervoxel segments) and StreamGBH (black). {\bf Right}: Compactness metric for our method and StreamGBH.}
	\label{fig:strands}
\end{figure*}

% --------------------------------------------------------------------------

\section{Supervoxel graph and segmentation}
\label{sec:segments}

Our video segmentation technique consists of two main steps: construction of the weighted supervoxel graph and its segmentation using spectral methods.

First, for each new frame at time $t$ of the RGB-D video stream, temporal-stable, depth-adaptive superpixels $S^{(t)}=\{s_i^{(t)}\}$ are computed and combined to a graph $G=(V,W)$ of depth-adaptive supervoxels where each supervoxel $v \in V$ consists of a series of superpixels $v = (s_{j_1}^{(T-n+1)},\ldots,s_{j_n}^{(T)})$, thus forms a supervoxel of strand-like shape (see fig. \ref{fig:strands}).

The last superpixels $S^{(t-1)}:=\{s_i^{(t-1)}\}$ of active supervoxels, i.e. supervoxels where $T = t-1$, are used to compute the superpixel density $\rho_C(x|S^{(t-1)})$.
Supervoxels are continued by trying to attach each new superpixel $s_j^{(t)}$ to the supervoxel which provided the seed point for the sampling and clustering process (see $\S$\ref{sec:stable}).
We assure that
\begin{equation}
	v=(\ldots,s_i^{(t-1)}, s_j^{(t)}) \;\text{iff.}\; \|s_i^{(t-1)} - s_j^{(t)}\|_{\mathcal{D}} \le \theta
	\label{eq:continue}
\end{equation}
where $\|\cdot\|_{\mathcal{D}}$ is a metric on the superpixel features, i.e. colour, spatial position and normal.
If a supervoxel can not be continued, the superpixel starts a new supervoxel in the supervoxel graph.
If during the sampling process a superpixel seed is deleted, the corresponding supervoxel in the supervoxel graph is closed and if a new seed is created, a new supervoxel is started (see fig. \ref{fig:strands}).

The weight $W_{ij}$ of an edge connecting now active supervoxels $i,j$ is updated using an exponential decay model:
\begin{equation}
	W_{ij}^{(t)} = (1 - \alpha) \, W_{ij}^{(t-1)} + \alpha \, n_{ij}^{(t)}
\end{equation}
where $n_{ij}$ is the corresponding superpixel similarity value from the current superpixel neighbourhood graph (see \cite{weikersdorfer2012dasp}).

Second, a spectral graph segmentation technique \cite{shi2000ncut, arbelaez2011contour} is used to segment the supervoxel graph.
The segmentation method is identical to the spectral graph technique for RGB-D image segmentation using depth-adaptive superpixels in \cite{weikersdorfer2012dasp} where the general eigenvalue problem
\begin{equation}
	(D - W) \, x = \lambda \, D \, x \;,\;\; D_{ii} = \sum\nolimits_j{W_{ij}}
	\label{eq:eigen}
\end{equation}
is solved, with $W$ the symmetric adjacency matrix of the supervoxel graph.
We limit the maximum number of supervoxels in the graph by excluding old supervoxels to assure a reasonable runtime of the spectral graph segmentation process.

The eigensystem solution of eq. \ref{eq:eigen} is used as in \cite{weikersdorfer2012dasp} to compute graph edge weights which form an ultrametric contour map (UCM) \cite{arbelaez2011contour}.
The UCM is thresholded and processed in an automatic semi-supervised label propagation step as in \cite{xu2012streaming} to compute supervoxel segment labels which are temporally stable relative to the labelling from previous timesteps.
%This is a remarkable fact as it demonstrates that image segmentation techniques can be used for object tracking by translating the tracking problem into a segmentation problem of a spatiotemporal graph.

% --------------------------------------------------------------------------

\begin{figure*}[t!]
	\centering
	\includegraphics[height=3.56cm]{explain/explain1.jpg}
	\includegraphics[height=3.56cm]{explain/explain2.jpg}
	\includegraphics[height=3.56cm]{explain/explain3.jpg}
	\includegraphics[height=3.56cm]{explain/explain4.jpg}
	\caption{{\bf Left}: Colour and depth input. {\bf Middle left}: Supervoxels (mean colour) and supervoxel neighbourhood graph (lighter colour is higher similarity). {\bf Middle right}: Supervoxels (random colour) and supervoxel segments generated by our two-layer method. {\bf Right}: Selected layers of the 21-layer hierarchy generated by StreamGBH. $n$ is number of segments in the complete image.}
	\label{fig:explain}
\end{figure*}

\newcommand{\figeval}[1]{\includegraphics[width=0.121\textwidth]{#1}}

\begin{figure*}[t!]
	\centering
	% \figeval{labels/Alexander_water/m_1.jpg}
	% \figeval{labels/Alexander_water/m_2.jpg}
	% \figeval{labels/Alexander_water/m_3.jpg}
	% \figeval{labels/Alexander_water/m_4.jpg}
	% \figeval{labels/Alexander_water/m_5.jpg}
	% \figeval{labels/Alexander_water/m_6.jpg}
	% \figeval{labels/Alexander_water/m_7.jpg}
	% \figeval{labels/Alexander_water/m_8.jpg}
	% \figeval{labels/Alexander_water/m_8.jpg}
	% \figeval{labels/Alexander_water/m_9.jpg}
	% \figeval{labels/Alexander_water/m_10.jpg}
	% \figeval{labels/Alexander_water/m_11.jpg}
	% \figeval{labels/Alexander_water/m_12.jpg}
	% \figeval{labels/Alexander_water/m_13.jpg}
	% \figeval{labels/Alexander_water/m_14.jpg}
	% \figeval{labels/Alexander_water/m_15.jpg}
	\figeval{labels/Alexander_coffee2/m_1.jpg}
	\figeval{labels/Alexander_coffee2/m_2.jpg}
	\figeval{labels/Alexander_coffee2/m_3.jpg}
	\figeval{labels/Alexander_coffee2/m_4.jpg}
	\figeval{labels/Alexander_coffee2/m_5.jpg}
	\figeval{labels/Alexander_coffee2/m_6.jpg}
	\figeval{labels/Alexander_coffee2/m_7.jpg}
	\figeval{labels/Alexander_coffee2/m_8.jpg}
	\\\vspace{0.05cm}
	\figeval{labels/Alexander_rotatingcam1/m_1.jpg}
	\figeval{labels/Alexander_rotatingcam1/m_2.jpg}
	\figeval{labels/Alexander_rotatingcam1/m_3.jpg}
	\figeval{labels/Alexander_rotatingcam1/m_4.jpg}
	\figeval{labels/Alexander_rotatingcam1/m_5.jpg}
	\figeval{labels/Alexander_rotatingcam1/m_6.jpg}
	\figeval{labels/Alexander_rotatingcam1/m_7.jpg}
	\figeval{labels/Alexander_rotatingcam1/m_9.jpg}
	\\\vspace{0.05cm}
	\figeval{labels/box-double-1/m_3.jpg}
	\figeval{labels/box-double-1/m_4.jpg}
	\figeval{labels/box-double-1/m_6.jpg}
	\figeval{labels/box-double-1/m_7.jpg}
	\figeval{labels/box-double-1/m_8.jpg}
	\figeval{labels/box-double-1/m_9.jpg}
	\figeval{labels/box-double-1/m_10.jpg}
	\figeval{labels/box-double-1/m_11.jpg}
	\caption{
	Input colour images (first row), segmentation results from our method (second row) and from StreamGBH (third row).
	{\bf First scenario}: Hand and object movements with textured surfaces and cluttered background.
	{\bf Second scenario}: Camera movement and rotation.
	{\bf Third scenario}: A challenging scenario with inter-object occlusions and changes in object lighting.
	}
	\label{fig:results}
\end{figure*}

% --------------------------------------------------------------------------

\section{Evaluation}
\label{sec:evaluation}

We compare our method against the state-of-the-art hierarchical streaming video segmentation method StreamGBH \cite{xu2012streaming}.
While StreamGBH generates a deep hierarchy of superpixels of increasing size, our method only uses two layers: supervoxels and supervoxel segments (see fig. \ref{fig:explain}).

We report results under two well-established metrics for superpixels and segmentations: Explained variation and compactness.
The explained variation metric \cite{xu2012evaluation, moore2008} is
\begin{equation}
	R^2 = \frac{\sum_i{\left\|\mu_i - \overline{\mu}\right\|^2}}{\sum_i{\left\|x_i - \mu_i\right\|^2}}
\end{equation}
with $x_i$ pixel value for pixel $i$, $\mu_i$ mean value for corresponding superpixel, $\overline{\mu}$ mean value over all pixels.
It can show a correlation to human annotations in certain scenarios \cite{erdem2004performance} and is reported for colour and depth.
Superpixel compactness is computed using the isoperimetric quotient \cite{schick2012compact}.
% \begin{equation}
% 	\sum_i{
% 		\frac{A_i}{A}
% 		\frac{4\pi A_i}{L_i^2}
% 	}
% \end{equation}

In fig. \ref{fig:strands} we compare our two-layer method for varying UCM edge thresholds (blue line) and varying superpixel numbers (red line) against the multi-layer method StreamGBH on a dataset consisting of six video sequences.
Our method generates segments which are both compact and have a high value for explained colour variation, in contrast to StreamGBH which overfits on colour information at the cost of compactness.
In addition, our method achieves significantly better results for explained depth variation which is a strong indicator for segmentations which respect geometry borders and thus perform better overall.
A close-up comparison in fig. \ref{fig:explain} demonstrates the shortcomings of StreamGBH regarding overfitted superpixels and unmet depth boundaries which our method does not possess.
%Additional spatial radius and spatial length of supervoxels and the percentage of discontinued supervoxels (see eq. \ref{eq:continue}) and added resp. removed superpixels during sampling (see sec. \ref{sec:stable}) is reported.

In fig. \ref{fig:results} we report segmentations for our method and StreamGBH for three challenging scenarios and demonstrate that our method can handle difficult situations with fast motions, partial occlusions, textured objects and lighting changes.

As the supervoxel graph is changing gradually over time, it is not necessary to compute the graph segmentation step for every frame.
%Moreover supervoxel graph construction is not dependent on segmentation, thus the spectral problem can be solved in parallel to supervoxel construction.
Computing segmentations every fourth frame, our method has a near realtime performance of 0.38 seconds per frame compared to 71.4 seconds per frame for StreamGBH.

More labelling results and the source code of our DASV implementation can be found on the project homepage \footnote{Project homepage: \url{https://github.com/Danvil/dasv}}.

% --------------------------------------------------------------------------

\section{Conclusion}
\label{sec:conclusion}

We proposed depth-adaptive supervoxels, a segmentation technique for RGB-D video streams which respects both temporal and spatial coherence and applied it to streaming video segmentation. 
Our evaluation showed that we outperform the state of the art for explained depth variation and compactness with significantly better runtimes.
%In addition, our qualitative results show a high stability even for moving objects.

% We proposed depth-adaptive supervoxels, a segmentation technique for RGB-D video streams which respects both temporal and spatial coherence.
% %Evaluation showed that the spatiotemporal segments align well with objects observed in the video, both for each frame as well as for frame stacks.
% By applying the proposed DASV in a tracking-by-segmentation framework, we showed that this technique can implicitly solve a complex task like tracking hand and object movements in 3D by segmentation of a spatiotemporal supervoxel graph.

% -------------------------------------------------------------------------

\bibliographystyle{IEEEbib}
\bibliography{danvil}

\balance

% --------------------------------------------------------------------------

\end{document}
