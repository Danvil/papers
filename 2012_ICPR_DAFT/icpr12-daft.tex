%
%  $Description: Author guidelines and sample document in LaTeX 2.09$
%
%  $Author: ienne $
%  $Date: 1995/09/15 15:20:59 $
%  $Revision: 1.4 $
%

\documentclass[times, 10pt,twocolumn]{article}
\usepackage{latex12}
\usepackage{times}
\usepackage{balance}

\usepackage[utf8]{inputenc}

\usepackage{textcomp}


\usepackage{graphicx}
\usepackage{amsmath,amssymb}
%\usepackage{ruler}
%\usepackage{color}

\usepackage{subfigure}

\usepackage{placeins}


\input{defines.tex}
\graphicspath{{./images/}}


%\documentstyle[times,art10,twocolumn,latex8]{article}

%-------------------------------------------------------------------------
% take the % away on next line to produce the final camera-ready version
\pagestyle{empty}

%-------------------------------------------------------------------------
\begin{document}

\title{%
Distinctive Texture Features from Perspective-Invariant Keypoints%
%Über-SIFT: Viewpoint-Invariant Features using Texture-Space Analysis
}

\author{David Gossow, David Weikersdorfer, Michael Beetz\\
\emph{Intelligent Autonomous Systems Group, Technische Universität München.}\\
\emph{[gossow|weikersd|beetz]@cs.tum.edu}\\
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
%\and
%Second Author\\
%Institution2\\
%First line of institution2 address\\ Second line of institution2 address\\
%SecondAuthor@institution2.com\\
}

\maketitle
\thispagestyle{empty}

\begin{abstract}

In this paper, we present an algorithm to detect and describe
features of surface textures, similar to SIFT and SURF.
In contrast to approaches solely based on the intensity image,
%it uses the depth information provided by an RGB-D camera
it uses depth information
to achieve invariance with respect to arbitrary changes
of the camera pose.

The algorithm works by constructing a scale space representation
of the image which conserves the real-world size and shape of texture features.
In this representation, keypoints are detected using a 
Difference-of-Gaussian response.
%, while responses along intensity edges
%and depth discontinuities are omitted.
Normal-aligned texture descriptors are then computed from the
intensity gradient, normalizing the rotation around the
normal using a gradient histogram.

We evaluate our approach on a dataset of planar textured scenes and show
that it outperforms SIFT and SURF under large viewpoint changes.

\end{abstract}


%-------------------------------------------------------------------------
\Section{Introduction}

The detection of local image features that are invariant to changes
in scale and rotation, such as SIFT \cite{\sift} and SURF \cite{\surf},
have become a standard approach in many computer vision applications. However,
these algorithms are not invariant with respect to large changes
of camera viewpoint, which introduce more complex distortions of the image.
Affine invariant detectors as the one described in \cite{Mikolajczyk2002}
estimate a local invariant affine transformation from the intensity
image, however they require an iterative normalization
step that introduces additional instability and lowers distinctiveness.

The availability of cameras that provide accurate depth information
for most pixels makes it possible to circumvent this problem,
using the surface normals to locally undistort the image.
Recently, several such methods have been proposed.
In \cite{Koser2007}, regions are detected using a purely image-based
approach, and then rendered from a normal-aligned viewpoint
for the descriptor computation. In \cite{Clipp2008}, the
scene is first decomposed into planar regions. Then, SIFT 
is applied to each perspectively normalized plane.
The approach described in \cite{Zaharescu2009} incorporates
the full 3D information directly into the detection and description
steps, however it requires a uniformly sampled triangular
mesh of the scene.

%In the following sections, we describe an algorithm
%that extends existing detection and description schemes to incorporate 
%the depth information, making it invariant to arbitrary changes in camera pose.
%It works directly on a intensity and depth image without further assumptions
%about the scene geometry.

In the following sections, we describe an algorithm
that achieves invariance to arbitrary changes in camera pose,
%by using the depth information.
working directly on a intensity and depth image without further assumptions
about the scene geometry.
In section \ref{sec:eval}, we evaluate the approach against SIFT and SURF
and show that it exhibits the desired properties.


\section{Feature Detection}

\begin{figure}
\includegraphics[width=\columnwidth]{screenshots/kps}
\caption{Keypoints detected using our approach}
\label{fig:teaser}
\end{figure}


\iffalse
\begin{figure*}
\label{fig:detection}

\subfigimg{screenshots/level1}{Gaussian filtered image for \mbox{$\worldsc=0.025m$}}%
\subfigimg{screenshots/level2}{Gaussian filtered image for \mbox{$\worldsc=0.05m$}}%
%
\subfigimg{screenshots/max_kp}{Difference-of-Gaussians response}%
\caption{Two consecutive layers of the scale space representation (a,b) and
the resulting Difference-of-Gaussians response (c).
%Substraction of two consecutive 
%scale levels (a,b) yields the Difference-of-Gaussians (DoG) response (c), in which
%extrema are detected. Only extrema are detected, for which the response
%of the DoG response as well as the ratio of principal curvatures
%is above a given threshold.
}
\end{figure*}
\fi

For the detection of texture features, we adapt the detection scheme
of SIFT \cite{\sift}. However, in order to remove the need to search and
interpolate maxima in the scale dimension, we convolve the image with a Gaussian
of a fixed real-world size at each scale level. Using this paradigm, it
is sufficient to double the size of the Gaussian between two scale levels.

For a given image of size $w\times h$,
the range of world scales is determined from
the depth image in order to yield pixel scales in the range of $[3,min(w,h)/20]$.
However, depending on the application domain, this choice can be replaced by a
fixed range.

The local projection of the Gaussian into image space
is approximated using an affine transformation and the convolution
computed using the Feline algorithm \cite{feline}.
%which is a standard approach for anisotropic texture filtering in computer graphics.
%In contrast to previous affine invariant detectors \cite{Mikolajczyk2002},
%which estimate the affine transformations from the intensity image,
We compute the affine parameters at each pixel
from the depth gradient and take a number of equally-weighted
box-shaped samples along the major axis using an integral image \cite{\surf},
as shown in figure \ref{fig:feline}.

%\FloatBarrier

\begin{figure}
\center
\includegraphics[width=0.3\columnwidth]{feline-sampling}
\caption{Gaussian approximation using box-shaped samples}
\label{fig:feline}
\end{figure}

In order to compute the local affine parameters,
we assume a pinhole camera model with
focal length $\f$ and and principal point $\fpoint$
such that a point $\ptc=(\xw,\yw,\zw)$
% in camera coordinates
is projected to pixel coordinates $(\xp,\yp)$ by
\begin{align}
(\xp,\yp) = \frac{ \f }{ \zw } \cdot (\xw,\yw) + \fpoint
%(\xp,\yp) = \f / \zw \cdot (\xw,\yw) + \fpoint
%(\xp,\yp) = \frac{ (\xw,\yw) \cdot \f }{ \zw } + \fpoint
\end{align}
%where $\f$ represents the camera's focal length
%and principal point $\fpoint$.

The orthogonal basis of the local affine transformation at world scale $\worldsc$ is then
given by its major axis $\affmajor$, and minor axis $\affminor$, where 
$\affminor$ is parallel to the depth gradient $\depthgrad$:
\begin{align}
%|\affmajor| = \frac{ \worldsc \f }{ \zw },~
|\affmajor| = \frac{ \f }{ \zw } \cdot \worldsc,~~
%|\affmajor| = \f / \zw \cdot \worldsc,~
|\affminor| = \frac{|\affmajor|}{\sqrt{\big\|\depthgrad\big\|^2 + 1}}
\end{align}

%Choosing $c =  0.7355$ results in the zero crossings of the
%Difference-of-Gaussians to lie on a
%circle of radius $\worldsc$, thus yielding the maximum response for a
%texture feature of that size.

In order to achieve similar invariance properties with respect to the 
depth gradient $\depthgrad$, the depth map is smoothed using $|\affminor| = |\affmajor|$.
%Image locations where the curvature of the filtered
%depth map exceeds a given threshold are excluded from the following steps,
%eliminating responses along object borders and strongly-curved surfaces.

Each pixel that is a local extremum in the elliptical neighbourhood
of size $2\worldsc$ in the difference $\dog(\worldsc)$ 
of two consecutive layers generates a keypoint.
%Keypoints are generated at positions that are a local extremum 
%in the difference $\dog(\worldsc)$ of two consecutive layers.
%Local neighbourhoods are computed from the
%affine transformation, corresponding to a circle of radius $2\worldsc$.
%In addition to a threshold on the value of $\dog$, we compute the
%ratio of princial curvatures in order
%to suppress unstable responses along intensity edges\cite{\sift}.
In addition to a threshold on the value of $\dog$, 
we suppress unstable responses along intensity edges
using the princial curvature ratio\cite{\sift}.
Figure \ref{fig:teaser} shows an example of keypoints that were
detected using this algorithm.

\section{Descriptor Computation}

\begin{figure}
\begin{minipage}[t]{\columnwidth}\center
%\smallimg{screenshots/ori-hist}%
\smallimg{screenshots/desc-sampling}
\hspace{.5cm}
\smallimg{screenshots/desc-points}
\end{minipage}

\caption{Sample locations and the corresponding points in tangential
coordinates used for the descriptor computation}
\label{fig:descriptor}
\end{figure}

We use the scheme of SURF \cite{\surf} to compute keypoint descriptors.
%In order to deal with surfaces that are not perfectly planar,
%we use the positions of samples projected onto the local tangent plane
%instead of their image positions.
In order to do that, we first need to evenly sample 
intensity gradient values in the neighbourhood of each keypoint
$\kp=(\ptc,\worldsc)$.

Assuming local planarity,
we project evenly spaced locations from the tangent plane into image space
and compute the intensity gradients between neighbouring
samples. For weighting samples and distributing them into the descriptor bins,
we then transform their 3D coordinates into the local tangent space $\tang$.

We compute $\tang$ by sampling
depth values from a $3 \times 3$ grid of side length $4\worldsc$
using the previously computed local affine approximation.
From the corresponding 3D points, the tangential plane
and normal axis are computed using principal component analysis.

To normalize the rotation of the tangent plane,
we sample a circular neighbourhood with radius
$5\worldsc$ and step size $0.5\worldsc$ in the Gaussian filtered intensity image 
$\scimg(2\worldsc)$ and compute the discrete intensity
gradients. All gradients are then weighted with a 3D Gaussian
centered at the keypoint location and $\sigma = \frac{5}{3} \worldsc$.
When computing the distance to the keypoint center,
the position along the normal is scaled by a factor of 5,
decreasing the weight of samples that lie outside of the
tangent plane.
The dominant orientation is computed
using an orientation histogram as described in \cite{\surf}.

%In order to gain robustness towards non-planarity of
%the local neighbourhood, the corresponding points $\ptc_i, i \in [1\ldots N]$
%in camera space are transformed to $\tang$, yielding a set of points $\ptt_i$.
%The discrete intensity gradient in this space is
%approximated by differences of neighbouring points, assuming even spacing.
%All gradients are then weighted with weights
%$\weight_i = |\ptt_i / (10\worldsc)|^2$, and collectd in an orientation
%histogram (see figure \ref{fig:ori-computation}). 
%From this, the dominant orientation is computed as described in 
%\cite{\surf}.

The sampling and gradient computation for the descriptor is done in the 
same way (fig. \ref{fig:descriptor}),
this time considering a window of side length $20\worldsc$
in $\scimg(\worldsc)$
using $\worldsc$ as step size. The gradients are weighted
with a Gaussian with $\sigma = \frac{20}{3} \worldsc$ 
and distributed into $4 \times 4$ patches as in~\cite{\surf}.
In order to avoid border effects, 
each sample is distributed into the four neighbouring bins
using bilinear interpolation.
Each bin is then separately normalized by the
sum of bilinear weights of gradients that contributed to it,
which increases robustness towards missing depth information.
If one of the bins does not contain any information,
the keypoint is omitted.

When matching the feature vectors, the search space
can be significantly reduced in comparison to purely
image-based descriptors by taking into account the
real-world scale.

%As each keypoint is detected at a distinct real-world scale,
%the search space for matching the feature vector can be
%significantly reduced in comparison to purely image-based descriptors.

%\FloatBarrier

\section{Evaluation}
\label{sec:eval}

We use the same evaluation framework as in \cite{Mikolajczyk2002} to compare our
approach (dubbed Depth-Adaptive Feature Transform, or DAFT)
to SIFT \cite{Lowe2004DIF} and SURF \cite{Bay2006SSU}.
We created multiple datasets using the Microsoft Kinect sensor at an image
resolution of $1280 \times 960$, with the depth image upscaled to the same
resolution. Each one shows the same planar scene under a change of the camera's
perspective, such as the angle between camera axis and plane normal (\emph{viewpoint}),
the rotation around the plane normal (\emph{rotation}) or the distance
of the camera to the scene (\emph{scaling}).
In order to test with partially planar scenes,
we implemented a step in the repeatability evaluation to omit points outside of
the planar image area using a manually created mask.
During matching, the nearest neighbour distance ratio is used as the
rejection criterion.

%Ground truth is generated by manually specifying point correspondences 
%between images and using a small-baseline matching algorithm to estimate
%a homography between images, which restrict the evaluation to planar scenes.
%Ground-thruth for keypoint correspondences is determined by computing the overlap
%of their corresponding elliptical regions.

We evaluate the detector by means of repeatability
and the descriptor by means of precision/recall 
(for a definition, see \cite{Mikolajczyk2002}).
In addition, we compute the maximal $F_1$ score from each precision/recall
graph in order to show the decay of descriptor performance with
respect to the strength of an image transformation, where
% the f1 score is defined as follows:
\begin{align*}
%\text{repeatability} &= \text{\#correspondences } \div \min(n_1,n_2)
F_1 := 2 \cdot \text{precision} \cdot \text{recall} \div (\text{precision} + \text{recall})
\end{align*}

%The values are computed as follows ($n_1$ and $n_2$ refer to the number
%of keypoints found in a pair of images):
%\begin{align*}
%\text{repeatability} &= \frac{\text{\#correspondences}}{\min(n_1,n_2)}
%\text{repeatability} &= \text{\#correspondences } \div \min(n_1,n_2)
%\\
%\text{precision} &= \text{\#correct matches } \div \text{ \#matches}
%\\
%\text{recall} &= \text{\#correct matches } \div \text{ \#correspondences}
%\\
%\text{f1 score} &= 2 \cdot \text{precision} \cdot \text{recall} \div (\text{precision} + \text{recall})
%\end{align*}
%
%For more details on the evaluation criteria, see \cite{Mikolajczyk2002}.

Since the evaluated algorithms detect a similar type of feature, we adjusted
the thresholds of the detectors to yield between 300 and 500 features for
the first image in each data set, depending on the scene.
We adjusted the size of the region
assigned to a keypoint with respect to its scale
to be equal for all algorithms using artificial images.

%Figure \ref{fig:results} shows the results we obtained from the evaluation,
%where the algorithm descibed in this paper was dubbed Depth-Adaptive
%Feature Transform (DAFT).

%When 500 keypoints are copmputed,
The average run times on a 2.7 GHz CPU are 0.1s for SURF and 0.6s for SIFT.
The run time of DAFT varies between 1.2s and 2.2s,
since the number of probes necessary for the Gaussian approximation
depends on the surface tilt.

Figure \ref{fig:results} shows the evaluation results.
As to be expected, results for the different approaches are similar
in case of a rotation around the camera axis and scaling,
while the noisiness of the depth information negatively affects repeatability
of DAFT when the camera moves far away from the scene.
In the case of large viewpoint changes,
the additional invariance properties of DAFT
strongly improve its repeatability
in comparison to SIFT and SURF.
%our algorithm
%with respect to perspective distortions 
%This results in little
%decline in repeatability and descriptor performance, 
%even in the case of strong perspective distortions.
%, when SIFT and SURF completely fail.

Note that, since SIFT and SURF generate circular image regions
and the evaluation computes corresponding regions by how much the projected regions
overlap, SIFT and SURF can not generate correspondences at a viewpoint angle
of 60\textdegree~and a rotation of more than 45\textdegree~around the surface normal.
We found that one reason for the repeatability going down even for DAFT
is that some features become too small to be detected, and that for some features
that become larger in the image, the corresponding descriptor
window exceeds the image boundaries or contains too much missing depth information.

While the test set is relatively small, it demonstrates that the algorithm's
principle works as expected, increasing detection and matching
performance under large changes of camera perspective.

\section{Conclusion}

We have presented a novel algorithm
that extracts viewpoint-invariant texture features from an image
by incorporating depth information into both it's
detection and description step.
%
%which are invariant with respect to the camera pose
%for the detection and
%description of texture features
%which are invariant with respect to the camera pose. This is achieved
%by incorporating depth information into all phases
%the information provided by a dense depth sensor.
%In this paper, we have introduced a novel algorithm for the detection and
%description of texture features
%which are invariant with respect to the camera pose. This is achieved
%by incorporating depth information into all phases
%the information provided by a dense depth sensor.
%
In our evaluation, we have demonstrated that it clearly outperforms SIFT
and SURF under large viewpoint changes.

While we extend the detection scheme of SIFT and the description
step of SURF to 3D surface textures, the described framework can be applied to achieve
the same invariance properties for other feature extraction schemes.

The source code and data sets used in this paper are available at
\emph{https://ias.in.tum.de/people/gossow/rgbd}.

\begin{figure*}[ht]
\label{fig:results}

\vspace{1cm}
\hfill\includegraphics[width=0.25\columnwidth,trim=5cm 15.7cm 7.5cm 5cm,clip=true]{legend}%
\hspace{1.5cm}
\vspace{-2.5cm}

\result{world_map/rotate0_1}{1}{12}{11}%
\result{granada/rotate40_1}{1}{11}{10}%
\result{granada/rotate60_1}{1}{17}{16}%
%\result{world_map/rotate0_1}{1}{12}{6}{11}%
%\result{tum_poster/viewpoint}{1}{8}{7}%
%\result{world_map/viewpoint0_1}{1}{10}{9}%
\result{world_map/viewpoint45_1}{1}{10}{9}%
\result{frosties/viewpoint}{1}{8}{7}%
\result{world_map/scale_2}{1}{8}{7}%

\caption{Evaluation results for a set of textured scenes. 
Each row contains from left to right: First and last image
of the sequence, repeatability and number of correspondences
(feature detection), maximal f1 scores for all images
and precision/recall for the last image in the sequence
(descriptor matching). The first 3 datasets contain
a rotation around the surface normal at a viewpoint angle of 0\textdegree, 
40\textdegree and 60\textdegree, 
followed by two datasets with increasing viewpoint angle and
one with the camera moving away from the planar surface.
}
\end{figure*}


%-------------------------------------------------------------------------
\bibliographystyle{latex12}
\bibliography{icpr12-daft,eccv}

\end{document}

