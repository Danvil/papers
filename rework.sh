#/bin/bash

dir="paper_2012_icpr_daft"
tmp="${dir}_tmp"
keep="2012_ICPR_DASFT"

git clone papers_git/ $tmp
cd $tmp
git remote rm origin

git tag -d icip_submission
git filter-branch -f --prune-empty --index-filter \
'git rm --cached -qr -- . && git reset -q $GIT_COMMIT -- '"$keep"
#git filter-branch -f --prune-empty --tree-filter \
#'find . ! -name "." ! -name '"$keep"' ! -name ".git" -maxdepth 1 -exec rm -rf {} \;'

git mv $keep/* .
git mv $keep/.gitignore .
rmdir $keep
git commit -am "Moved files to base directory"

rm -rf .git/refs/original
git reflog expire --expire=now --all
git gc --prune=now
git gc --aggressive --prune=now

cd ..

git clone $tmp $dir
cd $dir
git remote rm origin
git remote add origin "ssh://git@bitbucket.org/Danvil/${dir}.git"
#git push -u origin --all

cd ..
rm -rf $tmp
