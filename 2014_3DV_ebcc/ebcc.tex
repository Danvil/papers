\documentclass[10pt,twocolumn,letterpaper]{article}

%-------------------------------------------------------------------------
\usepackage{cvpr}
\usepackage{times}
\usepackage{epsfig}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
%
\graphicspath{
	{./images/}
	{/home/david/Documents/Science/papers/2014_3DV_ebcc/}
}
%
\makeatletter
\newcommand{\Repeat}[1]{%
	 \expandafter\@Repeat\expandafter{\the\numexpr #1\relax}%
}
\def\@Repeat#1{%
	 \ifnum#1>0
		  \expandafter\@@Repeat\expandafter{\the\numexpr #1-1\expandafter\relax\expandafter}%
	 \else
		  \expandafter\@gobble
	 \fi
}
\def\@@Repeat#1#2{%
	 \@Repeat{#1}{#2}#2%
}
\makeatother
%
\usepackage{blindtext}
\usepackage{color}
\definecolor{gray}{gray}{0.5}
\newcommand{\dummyparagraphs}[1]{\Repeat{#1}{{\color{gray}\blindtext}\\}}
%
%-------------------------------------------------------------------------

\newcommand{\reffig}[1]{fig.~\ref{fig:#1}}
\newcommand{\Reffig}[1]{Fig.~\ref{fig:#1}}
\newcommand{\refeq}[1]{eq.~\ref{eq:#1}}
\newcommand{\Refeq}[1]{Eq.~\ref{eq:#1}}
\newcommand{\reftab}[1]{table~\ref{tab:#1}}
\newcommand{\Reftab}[1]{Table~\ref{tab:#1}}
\newcommand{\refalg}[1]{alg.~\ref{alg:#1}}
\newcommand{\Refalg}[1]{Alg.~\ref{alg:#1}}
\newcommand{\refdef}[1]{def.~\ref{def:#1}}
\newcommand{\Refdef}[1]{Def.~\ref{def:#1}}
\newcommand{\refsec}[1]{section \ref{sec:#1}}
\newcommand{\Refsec}[1]{Section \ref{sec:#1}}

\newcommand*\rfrac[2]{{}^{#1}\!/_{#2}}
\newcommand{\argmax}{\operatorname*{arg\,max}}
\newcommand{\argmin}{\operatorname*{arg\,min}}

\newcommand{\prob}{\mathcal{P}}
\newcommand{\probx}[2]{\mathcal{P}\left(#1\,|\,#2\right)}
\newcommand{\ef}{\mathcal{F}}
\newcommand{\polerr}{\epsilon}
\newcommand{\tnow}{t^{*}}
\newcommand{\polf}{\omega_\polerr}
\renewcommand{\vec}[1]{\mathbf{#1}}
\newcommand{\nd}[3]{\mathcal{N}(#1\,|\,#2,#3)}
\newcommand{\img}{U}
\newcommand{\realpos}{\mathbb{R}^{+}}
\newcommand{\plane}{\mathbb{R}^{2}}
\newcommand{\edge}{DE}
\newcommand{\edgeq}[1]{DE_#1}%{D_{E,#1}}
\newcommand{\oppedge}{DO}%{D_{E^{*}}}
\newcommand{\corn}{DC}
\newcommand{\ve}{\vec{e}}
\newcommand{\sumb}{N_\ve}

% Include other packages here, before hyperref.

% If you comment hyperref and then uncomment it, you should delete
% egpaper.aux before re-running latex.  (Or just hit 'q' on the first latex
% run, let it finish, and you should be clear).
\usepackage[pagebackref=true,breaklinks=true,letterpaper=true,colorlinks,bookmarks=false]{hyperref}


% \cvprfinalcopy % *** Uncomment this line for the final submission

\def\cvprPaperID{23} % *** Enter the 3DV Paper ID here
\def\httilde{\mbox{\tt\raisebox{-.5ex}{\symbol{126}}}}

% Pages are numbered in submission mode, and unnumbered in camera-ready
\ifcvprfinal\pagestyle{empty}\fi
\begin{document}

%-------------------------------------------------------------------------
% \title{Event-based camera calibration and a\\Dynamic Vision Sensor stereo vision dataset}
\title{A primer for event-based stereo matching:\\Event-based camera calibration and a ground truth stereo dataset}

\author{
	David Weikersdorfer\\
	fortiss GmbH\\
	Guerickestr. 25\\
	{\tt\small david.weikersdorfer@gmail.com}
% For a paper whose authors are all at the same institution,
% omit the following lines up until the closing ``}''.
% Additional authors and addresses can be added with ``\and'',
% just like the second author.
% To save space, use either the email address or home page, not both
	\and
	J\"org Conradt\\
	Technical University Munich\\
	Karlstr. 45\\
	{\tt\small conradt@tum.de}
	\and
	Alois Knoll\\
	Technical University Munich\\
	Boltzmannstr. 3\\
	{\tt\small knoll@tum.de}
}

\maketitle
% \thispagestyle{empty}


%-------------------------------------------------------------------------
\begin{abstract}
Dynamic Vision Sensors are a promising class of visual sensors which use an event-based data-driven sensor model.
These sensors have many advantages over frame-based image sensors and currently more and more classical computer vision algorithms are redeveloped for the event-based data model.
With this paper we want to initiate research on event-based stereo vision algorithms which form the basis of three-dimensional environment perception with Dynamic Vision Sensors.
We first present a novel event-based checkerboard detection algorithm which enables accurate computation of intrinsic camera parameters for Dynamic Vision Sensors.
Second we use a calibrated camera setup consisting of two Dynamic Vision Sensors and an RGB-D sensor to record a ground-truth annotated stereo vision dataset.
Each take in the dataset consists of the two rectified streams of pixel events together with ground truth disparity and depth values from the RGB-D sensor.
The dataset is released to the public as part of this publication.
\end{abstract}


%-------------------------------------------------------------------------
\section{Introduction}

Three-dimensional perception of the environment is an important requirement for almost any task involving reasoning about or manipulation of the world around us.
Important example applications are autonomous vehicles, industrial automation, micro aerial vehicles, or robots for assisting in the care of elderly or handicapped people.
A major endeavor in three-dimensional perception are stereo matching algorithms which try to find pixel correspondences in two images taken from similar viewpoints \cite{scharstein2002taxonomy,mei2011building,klaus2006segment}.
By computing pixel disparities for matches, triangulation can be used to compute corresponding 3D points and depth values.
Other non-visual approaches are using active sensors like lidar or radar \cite{montemerlo2008junior,urmson2008autonomous} which are however relatively expensive and suffer from low angular resolution.
Another possible approach which has come to recent wide-spread use is the PrimeSense RGB-D sensor \cite{shotton2011bodypartrecognition} which uses structured light to compute depth values.
This family of sensors is however currently limited to indoor use and has difficulties with various material properties.
%Due to the limitations of non-visual sensors, stereo-matching algorithm have remained a key research focus.

\begin{figure}
	\centering
	\includegraphics[width=0.25\columnwidth]{teaser/events.png}%
	\includegraphics[width=0.25\columnwidth]{teaser/cme.png}%
	\includegraphics[width=0.25\columnwidth]{teaser/cb.png}%
	\includegraphics[width=0.25\columnwidth]{teaser/mapped.png}
	\includegraphics[width=\columnwidth]{teaser/scene.png}
	\caption{
		Top row: Event-based camera calibration - depicted are a checkerboard as seen by an DVS, checkerboard corner probability, detected checkerboard, and corresponding undistorted events.
		Bottom row: Example scene from our stereo vision dataset - depicted are undistorted events from DVS sensor, events with ground truth disparity, ground truth disparity image, and color image for reference.
	}
	\label{fig:teaser}
\end{figure}

\begin{figure*}
	\centering
	\includegraphics[width=\textwidth]{series/office_raw.png}
	\caption{
		Example for an event-based data stream and corresponding color frames. See \refsec{ebv} for explanation.
	}
	\label{fig:dvs_example}
\end{figure*}

Frame-based visual sensors have several inherent shortcomings especially in  low-latency applications and with respect to the huge computational power required to process the generated data.
Dynamic Vision Sensors (DVS) are an alternative visual sensor which aims to solve theses problems by using a data-driven approach instead of the time-driven approach of classic image sensors.
Instead of capturing complete photos at fixed time intervals, a DVS transmits only incremental changes in perceived luminance immediately when they appear.
This concept has several additional advantages which are further highlighted in section 2 of this paper.
While the research on event-based computer vision algorithms is already in full motion \cite{benosman2014event,weikersdorfer2012edvs,weikersdorfer2013edvsslam,conradt2009} stereo algorithms have not been investigated thoroughly, yet.

The research on stereo matching algorithms for frame-based cameras has been advanced by publicly available datasets which can be used to directly assess the performance of a potential new approach.
Examples of very successful stereo datasets are the Middlebury Stereo Datasets \cite{scharstein2002taxonomy} and the KITTI Vision Benchmark Suite \cite{geiger2012we}.
While the Middlebury dataset focuses on indoor scenes, the KITTI dataset focuses on autonomous vehicles and provides various outdoor and traffic scenarios.
For both datasets a key contribution are the accurate ground truth depth and disparity labelings.
In case of the KITTI dataset they are for example provided by an additional Velodyne laser scanner.
An important requirement for aligning color images from two stereo cameras among each other and with ground truth depth sensors is an accurate camera calibration.
One needs to compute intrinsic camera parameters, like focal length and lens distortion, and the relative pose of sensors towards each other.
% Additionally stereo cameras are normally rectified to simplify local patch matching by searching for correspondences only along the horizontal axis.

To initiate the research on event-based stereo vision algorithms for Dynamic Vision Sensors we present two contributions in this paper.
First we develop an event-based camera calibration algorithm which can detect inner corner points of a checkerboard.
Due to the different data model used by Dynamic Vision Sensors, classic approaches from frame-based vision can not be transferred easily. 
Our algorithm enables the convenient and accurate calibration of intrinsic camera parameters of Dynamic Vision Sensors and permits the integration of a DVS into existing sensor setups consisting for example of frame-based color or depth sensors.
Our second contribution is an annotated event-based stereo vision dataset.
We captured 69 takes of indoor scenarios with a hybrid sensor setup consisting of two Dynamic Vision Sensors and an RGB-D combined color and depth sensor.
All sensors are fully calibrated and rectified and events from the main camera are annotated with ground truth depth and disparity values.
Our dataset can be used freely to develop and evaluate future event-based stereo algorithms for Dynamic Vision Sensors.

This paper is structured as follows.
In \refsec{ebv} we present the event-based vision paradigm of Dynamic Vision Sensors in more detail.
In \refsec{ebcb} our proposed event-based checkerboard detection algorithm is presented and in \refsec{ebcc} we describe how it can be used for the calibration of Dynamic Vision Sensors.
Our dataset is introduced in \refsec{db} together with statistical information and sample takes from the dataset before concluding the paper in \refsec{conclusions}.


%-------------------------------------------------------------------------
\section{Event-based computer vision}
\label{sec:ebv}

Dynamic Visions Sensors \cite{lichtsteiner2008dvs} are a novel kind of biologically inspired vision sensor.
A classic frame-based vision sensor captures a sequence of photos at fixed time intervals.
The frame rate is often chosen as 30 or 60 frames per second with the rare exception of high speed cameras which provide low latency in the order of one millisecond or less, but are bulky, produce huge amounts of data and are very expensive.
Dynamic Vision Sensors use a novel data-driven approach to visual perception.
The environment is no longer measured at fixed time intervals, but chunks of information are created and transmitted only if changes are happening.
In a DVS each pixel operates independently from all other pixels and continuously integrates the luminance for its area on the sensor chip.
Only if a change in luminance is detected an event is generated and sent to the host computer.
Each event consists of a timestamp with microsecond accuracy, the pixel coordinate and a polarity bit which indicates if luminance increased (polarity 1) or decreased (polarity 0).
The operation of a DVS is motivated by the human retina which uses spiking neural networks to process visual stimuli.

\Reffig{dvs_example} shows a recording of a Dynamic Vision Sensor together with color images for reference.
The proper visualization of event-based data is not trivial due to the highly dynamic nature.
In this paper we choose a time-decayed visualization where events are collected over a short time, i.e. 500 milliseconds.
For each event a small rectangle or disk is rendered and its color is faded to a background color depending on the age of the event.
Polarity as in \reffig{dvs_example} is visualized by choosing white for a polarity of one, black for zero polarity and the background color is gray.
Other visualizations like in \reffig{dataset} use a color derived from depth or disparity which is faded to white.

Dynamic Visions Sensors have several exceptional advantages over frame-based vision sensors.
As data is only generated for changes in luminance and not in fixed time intervals, the sensor bandwidth requirements are reduced by at least two orders of magnitude.
An intelligent event-based computer vision algorithm can make use of this fact by providing decreased computation times and requiring less powerful and energy consuming processing hardware.
In a DVS each pixel fires events with microsecond time resolution enabling very low latency which could otherwise only be realized with a costly high-speed camera.
The independent operation of each pixel allows local gain control and thus a dynamic adaption to high lighting contrasts within one image.
Additionally it is possible to design hardware implementations of a DVS which has minimal size and energy consumption comparable to state-of-the-art frame-based vision sensors.
The Embedded Dynamic Vision Sensor \cite{conradt2009} has a pixel resolution of 128x128 pixels, a dynamic range or 120 dB, 15 microsecond latency, only consumes 23 mW of power, and has a size of 23x52mm (see \reffig{rig}).

It is by no means straight forward to transfer classic computer vision algorithms to Dynamic Vision Sensors.
To make full use of the sparse event-based data-driven sensor models new methods need to be developed to solve visual processing tasks.
In recent years development of event-based computer vision algorithms for Dynamic Vision Sensors has seen increasing activity.
In \cite{weikersdorfer2012edvs} an event-based particle filter algorithm was developed which allows general event-based tracking.
The algorithm was further developed in \cite{weikersdorfer2013edvsslam,hoffmann2013exploration,weikersdorfer2014dedvs} to a simultaneous localization and mapping algorithm.
Similar in \cite{ni2012asynchronous} an event-based iterative closest point algorithm for shape tracking was presented.
\cite{benosman2014event} presented an event-based optical flow algorithm and \cite{rogister2012asynchronous} a stereo matching algorithm based on event identification.


%-------------------------------------------------------------------------
\section{Event-based checkerboard detection}
\label{sec:ebcb}

Before starting with the presentation of the event-based checkerboard detection algorithm it shall be emphasized that classic computer vision algorithm can not be transferred directly to the event-based data model -- not even for basic processing tasks like checkerboard corner detection.
Events are captured with almost continuous timestamps and form a sparse image representation.
Primitive integration over a time window into an artificial image frame to enforce the precondition of classic computer vision algorithm subverts the main advantages of the data-driven model.
We therefor strive to develop event-based algorithms which do not use artificial framing, but can work on the event stream directly -- with the possible help of additional intelligent integration mechanisms.  

Our checkerboard edge and corner detectors shall find points which satisfy two criteria.
On the one hand edges should be near recent event, i.e. events with the highest possible timestamps.
On the other hand, event polarity shall match the desired checkerboard corner arrangement (\reffig{field_and_detectors}).
Our event-based checkerboard detection algorithm proceeds in the following steps.
The first detector $\edge$ is designed to react to current edges with homogeneous polarity.
$\edge$ is further developed into the detector $\oppedge$ for an edge with two segments of homogeneous but opposite polarity.
This forms the basis of the corner detector $\corn$ which checks for two such perpendicular bipolar edges.
\Reffig{field_and_detectors} visualized the patterns on which the three detectors react.
The detector is computed for every pixel in the image and the local maxima with highest detection result are searched.
Finally local maxima points are used in a corner sorting algorithm to localize the checkerboard in the image.
Details of the process are presented in the following.

\begin{figure}
	\centering
	\raisebox{-0.5\height}{\includegraphics[width=0.45\columnwidth]{edges/pm.png}}%
	\raisebox{-0.5\height}{\includegraphics[width=0.55\columnwidth]{edges/3d.png}}
	\caption{
		Left: Top left highlights polarities of events at a checkerboard corner points.
		The remaining images illustrate the four detectors $\edge$, $\oppedge$ and $\corn$.
		Right: Event time field for a checkerboard.
		Horizontal axes are sensor pixel coordinates and vertical axis is latest observed event time at that location.
	}
	\label{fig:field_and_detectors}
\end{figure}

As a basis of all three detectors we create an event field $\ef$ where at each pixel location $c \in \img$ of the sensor $\img$ we save timestamp and polarity of the latest event.
An example of an event time field is visualized in \reffig{field_and_detectors}.
It can be seen how timestamps occur continuously over time and thus events form a rather smooth tempo-spatial surface in device coordinates.

The first detector should react to an edge of homogeneous polarity which starts at a given pixel $c\in \img$ and goes into direction $d \in \plane$, $\|d\|=1$, for $R$ pixels.
We start by sampling a set of $R$ points from the event field $\ef$ which gives the sequence $\ve=(t_i,b_i)_{1 \le i \le R}$ with $t_i = \ef_t(c+i\,d) \in \realpos$ the time of the last event at this location and $b_i = \ef_b(c+i\,d) \in \{0,1\}$ the corresponding polarity.
For simplification, sampling is performed by rounding to the nearest pixel, but interpolated values can also be used in the following formulas.
The probability that $(t_i,b_i)$ lies on a current edge should depend both on how close the event time $t_i$ is to the current time $\tnow$ and if the polarity $b_i$ matches the desired polarity.
Similarity in time is modeled as 
\begin{equation}
	\probx{T{=}\tnow}{\ve} = \prod_{i=1}^n{\nd{t_i}{\tnow}{\sigma_t}}
	\label{eq:prob_t}
\end{equation}
where $\nd{\cdot}{\mu}{\sigma}$ is the normal distribution with mean $\mu$ and standard deviation $\sigma$.
The standard deviation $\sigma_t$ allows for a certain amount of deviation from the current time which can occur for example due to pixel discretization errors.
Polarity matching is modeled as
\begin{equation}
	\probx{B{=}1}{\ve} = \polf(\frac{\sumb}{n})^n,\, \probx{B{=}0}{\ve} = \polf(1 - \frac{\sumb}{n})^n
	\label{eq:prob_b}
\end{equation}
with $\sumb = \sum_{i=1}^n{b_i}$ and $\polf(x) = (1-\polerr)^{x} \, \polerr^{1-x}$.
$\polerr$ indicates the probability that an event accidentally has a wrong polarity for example due to sensor noise.
\Refeq{prob_b} can be derived from the condition
\begin{equation}
	\probx{B}{b} = \left\{
		\begin{array}{l l}
			1 - \polerr & \quad B=b\\
			\polerr & \quad B \ne b
		\end{array} \right.
	\label{eq:prob_polerr}
\end{equation}
The two probabilities from \refeq{prob_t} and \refeq{prob_b} are summarized into the probability function
\begin{equation}
	\edgeq{q}(c,d) = \probx{T{=}\tnow}{\ve} \, \probx{B{=}q}{\ve}
	\label{eq:prob}
\end{equation}

\begin{figure}
	\centering
	\includegraphics[width=0.33\columnwidth]{cb_details/events.png}%
	\includegraphics[width=0.33\columnwidth]{cb_details/oppedge.png}%
	\includegraphics[width=0.33\columnwidth]{cb_details/corner.png}\\
	\includegraphics[width=0.33\columnwidth]{cb_details/corner_time.png}%
	\includegraphics[width=0.33\columnwidth]{cb_details/corner_polarity.png}
	\caption{
		Top row: Raw events from sensor, $\edge$ detector response, $\corn$ detector response.
		Bottom row: $\corn$ detector only considering time, and $\corn$ detector only considering polarity.
		Detector response plots show high values in red.
	}
	\label{fig:detectors}
\end{figure}

In the second step we use the edge detector $\edgeq{q}$ to develop a checkerboard corner detector.
A checkerboard corner point is defined by the mid point of four rectangles where diagonal rectangles have identical coloring.
When the checkerboard is moved relative to the sensor, the strong intensity edges between black and white rectangles generate events while inside the rectangles only few spontaneous noise events are generated (see \reffig{field_and_detectors}).
There are four such edges around a checkerboard corner point and thus most events are expected to lie on these four lines.
The polarity of events on the edges depend on the movement direction and orientation of the checkerboard and can not be used for checkerboard detection in an absolute way.
However due to the alternating color of rectangles each pair of opposite edges always has opposite polarity.
By using the edge detector from \refeq{prob} we can formulate a detector $\oppedge$ for such an edge with opposite polarity.
\begin{equation}
\begin{split}
	\oppedge(c,d) = \max&\left(\edgeq{0}(c,d)\,\edgeq{1}(c,-d),\right. \\
	               &\;\left.\edgeq{1}(c,d)\,\edgeq{0}(c,-d)\right)
\end{split}
\end{equation}
There are two possibilities of polarity assignment and the permutation with the higher probability is used as detection probability.
As a checkerboard corner is defined by two perpendicular edges, the checkerboard corner detector is finally defined as:
\begin{equation}
\begin{split}
	\corn(c,d) = \oppedge(c,d) \, \oppedge(c,d^{\perp})
\end{split}
\end{equation}

The detector $\corn(c,d)$ can be used to compute the probability of a checkerboard corner for every image point $c \in \img$ and for a given orientation.
The overall probability of a corner independent from orientation is the maximum over all possible rotations:
\begin{equation}
	\corn(c) = \argmax_{0 \le \varphi < \rfrac{\pi}{2}}{\corn(c,(\cos \varphi, \sin \varphi)^T)}
	\label{eq:corn_argmax}
\end{equation}
\Reffig{detectors} shows response for the $\corn$ detector and the $\edge$ detector (both polarities).
The figure also shows the contributions of the time based detection and the polarity based detection.
The polarity term while being susceptible to noise on its own enhances detection by further increasing the peaks of the local maxima at corner points.

After applying the detector $\corn(c)$ to every pixel $c \in \img$ on the sensor the next task is the computation of local maxima.
This step is similar to the SIFT feature point detector \cite{lowe2004distinctive} where salient feature points are computed by finding local maxima in the scale space.
For each pixel we consider a rectangular neighborhood and mark that pixel as a local maximum if all for other pixel in the neighborhood $\corn$ gave a smaller value.
To get stable and unique checkerboard locations the size of the neighborhood should not be selected to small - we choose a rectangle width of 5 pixels.

The final task is the selection of the checkerboard corner points amongst the detected local minima and their arrangement into a regular grid.
The search for local minima can yield more points than expected for the given checkerboard and points can be in any order.
As our corner detector yields very distinctive results we directly select the points with highest detection probability for further processing.
Arranging these points into a grid is not trivial as the checkerboard may be heavily deformed by lens distortion.
% To arrange points into a regular pattern we first compute the average side length $\bar{s}$ of a checkerboard rectangle in pixels by averaging the mean distance to the four nearest neighbours for each point.
% As points on the corner and side increase the average distance by connecting to diagonal points we apply a correction factor:
% \begin{equation}
% 	\bar{s}_{\text{corrected}} = \bar{s}\,\frac{4\,w\,h}{4\,w\,h + (\sqrt{2}-1)\,2\,(w+h)}
% \end{equation}
For each point we know the corner orientation from \refeq{corn_argmax} and search in all four directions inside a cone for the nearest point.
If no such point is found the point is marked as a side or corner point.
The opening angle of the cone depends on the assumed maximum distortion of the checkerboard pattern.
The resulting connectivity is arranged into a lattice graph to extract the desired ordering of points.
At this point we may realize that the detected points do not form a valid checkerboard pattern and in that case the detection process is marked as failed.

We quickly discuss some implementation remarks for the detectors $\edge$ and $\corn$.
To simplify computation and avoid floating point inaccuracies due to multiplication of many probabilities it is sufficient to only compute the log-likelihood instead of the exact probability.
For example $\log \edgeq{q}(c,d)$ is computed for $q=1$, and similar $q=0$, as:
\begin{equation}
\begin{split}
	\log \edgeq{1}(c,d) &= \text{const.} - \frac{1}{2\,\sigma_t^2}\sum_{i=1}^R{(t_i - \tnow)^2} \\
	&+ \sumb \log(1-\polerr) + (n - \sumb) \log \polerr
	\label{eq:log_prob}
\end{split}
\end{equation}
The logarithm is a monotonically increasing function and thus preserves global and local maxima.
For the computation of $\corn(c)$ it is necessary to discretize over the angle $\varphi$.
Depending on the search range $R$ used for $\edge$ the number of tested orientations should not be too small.
Experiments have shown that $R=8$ and $8$ orientation samples for the interval $[0,\rfrac{\pi}{2}[$ give very good results.
Further, a choice of $\sigma_t = 0.1s$ and $\polerr=0.9$ works well in practice.
% Overall the implementation of $\corn$ is straightforward and is not very time consuming as it only involves several line scans over the event field for each pixel.


%-------------------------------------------------------------------------
\section{Calibration and event-based undistorting}
\label{sec:ebcc}

Camera calibration using a regular pattern like a checkerboard is a well studied topic \cite{zhang2000flexible} in computer vision and forms the basis of many camera calibration toolboxes for regular frame-based cameras.
The approach often involves the detection of a regular arrangement of salient points like checkerboard corner points in an image.
If the relative arrangement of theses points in 3D is known the established correspondences can be used to estimate intrinsic camera parameters like focal length, optical projection center and lens distortion coefficients using for example a maximum likelihood estimation.
The corresponding camera model equations are well known and are not repeated here.
The checkerboard detected by our event-based checkerboard detection algorithm developed in the previous section can be used directly for such an approach for example with the OpenCV computer vision toolbox \cite{Bradski:2000:OL}.

\begin{figure}
	\centering
	\includegraphics[width=0.33\columnwidth]{undistort_rnd/raw.png}%
	\includegraphics[width=0.33\columnwidth]{undistort_rnd/round.png}%
	\includegraphics[width=0.33\columnwidth]{undistort_rnd/random.png}
	\caption{
		Left: raw events from sensor.
		Middle: rounding undistorted event coordinates to pixel coordinates results in artifacts - red pixels are never hit by an event.
		Right: Probabilistic rounding removes artifacts.
	}
	\label{fig:undistortion}
\end{figure}

For further processing it is desirable to remove lens distortion from the visual data and in the following we propose a simple undistortion procedure suitable for Dynamic Vision Sensors.
For a normal image lens distortion is removed by mapping each pixel from the target, undistorted image back to the original, distorted image using the camera model equations and estimated parameters.
As mapped pixel coordinates are real values, interpolation is performed to avoid artifacts which would otherwise arise due to rounding errors.
The direction of the mapping is important: if the mapping would be performed from distorted to undistorted image, not all pixels in the undistorted image would be hit.
\Reffig{undistortion} demonstrates this effect by displaying raw and undistorted events, and pixels which are never hit by the mapping are marked in red.

For singular point events the original frame-based approach is infeasible for two reasons:
First when an event is reported in distorted image coordinates we need an immediate mapping from distorted to undistorted, and second, the interpolation of event timestamps and polarity does not make much sense.
If a subsequent event-based visual processing algorithm can handle real valued event coordinates this problem can be easily mitigated by directly forwarding the undistorted real valued coordinates.
For the case that integer coordinates are required, we propose to use probabilistic rounding of event pixel coordinates.
The pixel coordinate $i + p$, $i \in \mathbb{N}$, $p \in [0,1[$ is rounded to $i'$ after the formula
\begin{equation}
	i' = \begin{cases}
		i  & \text{if } r > p \\
		i+1 & \text{if } r \le p
	\end{cases} \text{ with } r \leftarrow \mathcal{U}(0,1)
\end{equation}
where $\mathcal{U}$ indicates a uniformly distributed random variable.
\Reffig{undistortion} depicts results for the probabilistic undistortion scheme and demonstrates that the rounding only adds minimal noise compared to normal rounding.

% \begin{figure}
% 	\centering
% 	\includegraphics[width=\columnwidth]{dummy.pdf}
% 	\caption{
% 		(opt) Unrectified / rectified eventstream side by side
% 	}
% 	\label{fig:}
% \end{figure}

% \begin{itemize}
% \item (opt) Camera pose tracking
% \item (opt) Evaluation of camera pose tracking using external tracker
% \end{itemize}


%-------------------------------------------------------------------------
\section{DVS stereo vision dataset}
\label{sec:db}

For the recording of our dataset we assembled a stereo camera setup consisting of two Embedded Dynamic Vision Sensors \cite{conradt2009} and an Asus PrimeSense RGB-D camera (see \reffig{rig}).
Our custom build housing provides mounts for both DVS sensors and is fixed to the housing of the Asus camera.
The baseline, i.e. the horizontal distance, between the two stereo DVS sensors is chosen as 12 cm which is a reasonable value for a range around 0.5-2 meters.
The two DVS sensors are connected to a mutual FTDI board which transmits data of both sensors over a single standard USB cable.
The sensors are additionally connected via a hardware link to provide hardware synchronization of event timestamps.
Synchronized timestamps are essential to enable accurate stereo matching.
The whole setup can be connected via two USB cables to a host computer.
To access the RGB-D camera we use the OpenNI framework, to access the Dynamic Vision Sensors we use a custom C++ driver -- jAER is an alternative publicly available Java toolbox.

To annotate events with disparity values we fuse depth values from the Asus RGB-D camera into the event stream recorded from the DVS sensors.
We operate the RGB-D sensor with a pixel resolution of 320x240 which enables a framerate of 60 frames per second. 
For each recorded depth image we use the Kinect camera parameters to compute 3D points from depth for each pixel.
Theses points are transformed into DVS sensor coordinates and projected onto the image plane for both DVS sensors.
We then store the depth and the pixel disparity, i.e. the distance between the horizontal coordinates of the two projections.
In case of multiple points hitting the same pixel due to the down-sampling or change of perspective, we keep the smaller depth value and the larger disparity value.
These images of reprojected depth and disparity can be used to annotate events.
For each new event we first undistort the event pixel coordinates and then look up depth and disparity in the previously prepared depth and disparity images.
Similar as in \cite{weikersdorfer2014dedvs} this method gives very good results for motions with reasonable velocity. 

For the presented dataset we record the following data for each take: a text file with a list of all events and sequence of images with color, depth and disparity images.
For each event we save the event timestamp with microsecond time resolution, the undistorted event pixel coordinates with sub-pixel accuracy value, the event polarity (either 0 or 1), the sensor id (0 for the right sensor, 1 for the left sensor), the ground truth depth (in m) and the ground truth disparity (in pixel).
Dense depth and disparity images for the right camera are stored as row major matrices in TSV files.
Frame file for depth, disparity and color are stored with a filename corresponding to the microsecond event timestamp of the most recent event. 
\Reffig{dataset} depicts the most important data streams which are provided for each take in the dataset.

\begin{figure}
	\centering
	\raisebox{-0.5\height}{
		\includegraphics[width=0.5\columnwidth]{rig/edvs_wt.jpg}}%
	\raisebox{-0.5\height}{
		\includegraphics[width=0.5\columnwidth]{rig/setup.pdf}}
	\caption{
		Left: Embedded Dynamic Vision Sensor. Right: Stereo camera rig used for recording our dataset.
	}
	\label{fig:rig}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{dataset/dataset.png}
	\caption{
		Data available in the dataset for each take. From left to right: Raw events from right and left DVS, events with annotated ground-truth disparity values, and depth and color images for reference.
	}
	\label{fig:dataset}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\columnwidth]{stats/stats.pdf}
	\caption{
		Statistics from our dataset: Sequence length (seconds) histogram, events/second histogram, depth value histogram, percentage black histogram.
		Each histogram shows relative frequency over the whole take or for individual events.
	}
	\label{fig:stats}
\end{figure}

\begin{figure*}
	\centering
	% \includegraphics[width=\textwidth]{series/office.png}
	\includegraphics[width=\textwidth]{series/smallplant-002.png}
	\includegraphics[width=\textwidth]{series/couch-003.png}
	\includegraphics[width=\textwidth]{series/person-door-001.png}
	\caption{
		Disparity annotated events and color image for selected takes from the dataset.
		Disparity is color encoded with high values in red and low values in blue. Events with missing depth values are marked in black.
	}
	\label{fig:dataset}
\end{figure*}

In total we have recorded 69 takes in 15 different scenarios.
\Reffig{dataset} shows three example sequences from the dataset.
Each take is around 8 to 14 seconds long and either involves a short camera movement around objects of interest or a static camera which observes moving entities.
Scenarios cover scenes with singular object, moving people and indoor rooms and each scenario consists of three to six similar takes.
\Reffig{stats} reports basic statistics for our dataset.
The depth values fall mostly in the near range between 0.5 and 1.5 meters, which is intentional as the depth resolution provided by the RGB-D sensor and the potential stereo matching quality is highest in this range.
The number of recorded events per second depends on the scene complexity and velocities and ranges from 30,000 to 130,000.
For some events no depth and disparity values are available as the RGB-D sensor can not provide depth values for objects with certain material properties (too reflective, too dark) or objects which are not in the overlapping viewport.
The percentage of such events is mostly lower than ten percent.


%-------------------------------------------------------------------------
\section{Conclusions}
\label{sec:conclusions}

Event-based vision sensors like the Dynamic Vision Sensor realize a data-driven approach to visual perception which has huge advantages for example for time-critical, embedded systems like micro aerial vehicles.
Where dense frames from frame-based vision sensors require huge computational power, the event-based sparse approach allows the design of much more efficient algorithms.
This is especially apparent for the stereo matching problem where many accurate algorithms are slow in computation as they have to use advanced techniques to optimize depth values for the many pixels on homogeneous surfaces with poor local matchings.
The development of event-based stereo matching algorithms which can focus on salient edges is thus a very promising future research field.

In this paper we provided a foundation for the advancement event-based stereo algorithms.
We solved the problem of accurate and reliable calibration of Dynamic Vision Sensors by presenting a checkerboard corner algorithm for event-based data streams.
This allows the easy integration of a Dynamic Vision Sensor into existing sensor setups.
Additionally we recorded a publicly available dataset using a stereo setup of Dynamic Vision Sensors which is annotated with ground truth depth and disparity values from an additional RGB-D sensor.
The dataset can be used in further research to forward the development and evaluation of new stereo matching algorithms.


%-------------------------------------------------------------------------
{\small
\bibliographystyle{ieee}
\bibliography{danvil}
}


%-------------------------------------------------------------------------
\end{document}
